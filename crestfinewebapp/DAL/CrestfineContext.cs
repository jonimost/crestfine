namespace crestfinewebapp
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;
    using crestfinewebapp.Models;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public partial class CrestfineContext : IdentityDbContext<UserLogin>
    {
        public CrestfineContext()
            : base("name=WelbeckDatabase", throwIfV1Schema: false)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AnnualReport> AnnualReports { get; set; }
        public virtual DbSet<MonthlyReport> MonthlyReports { get; set; }
        public virtual DbSet<WeekReport> WeekReports { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<AnnualReport>().HasKey<int>(x => x.AnnualReportID);
            builder.Entity<MonthlyReport>().HasKey<int>(x => x.MonthlyReportID);
            builder.Entity<WeekReport>().HasKey<int>(x => x.ReportID);
            /*builder.Entity<IdentityUserClaim>().ToTable("UserClaim").HasKey<Int32>(r => r.Id);
            builder.Entity<IdentityUserLogin>().ToTable("UserLogin").HasKey<string>(l => l.UserId);
            builder.Entity<IdentityRole>().ToTable("Role").HasKey<string>(r => r.Id);
            builder.Entity<UserLogin>().ToTable("User").HasKey(r => new { r.IDNumber, r.UserName });
            builder.Entity<IdentityUser>().ToTable("User").HasKey<string>(r => r.UserName);
            builder.Entity<IdentityUserRole>().ToTable("UserRole").HasKey(r => new { r.RoleId, r.UserId });
            */
        }
    }
}
