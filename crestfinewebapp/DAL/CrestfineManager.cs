﻿using crestfinewebapp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfinewebapp.DAL
{
    public class CrestfineManager: UserManager<UserLogin, string>
    {
        public CrestfineManager(IUserStore<UserLogin, string> store)
            :base(store)
        {

        }

        public static CrestfineManager Create(IdentityFactoryOptions<CrestfineManager> options, IOwinContext context)
        {
            var manager = new CrestfineManager(
                new UserStore<UserLogin>(context.Get<CrestfineContext>()));

            return manager;
        }
    }
}