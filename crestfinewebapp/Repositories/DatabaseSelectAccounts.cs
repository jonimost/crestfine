﻿using crestfinewebapp.Repositories;
using crestfinewebapp.Models;
using crestfinewebapp.Repositories.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace crestfinewebapp.Repositories
{
    /// <summary>
    /// Selects accounts from the database for making Reports
    /// </summary>
    public class DatabaseSelectAccounts : IDatabaseSelectAccounts<Reports>
    {
        private readonly CrestfineContext crestfineContext;
        public decimal? Payment_ { get; private set; }
        public decimal? Debt_ { get; private set; }
        public Int32 PurchasesCounter { get; set; }
        

        IDatabaseSave<Reports> databaseSave;

        public DatabaseSelectAccounts()
        {
            crestfineContext = new CrestfineContext();
            databaseSave = new DatabaseSaveReports();
        }

        public async Task<Reports> Selectannualrpts()
        {
            
            DateTime dateTime = DateTime.Today;
            Payment_ = 0.0M;
            Debt_ = 0.0M;
            PurchasesCounter = 0;
            var date = DateTime.Today.AddYears(-1);


            List<Account> annualAccount = await crestfineContext.Accounts
                    .Where(i => (i.Time >=date  && i.Time<=DateTime.Today)).ToListAsync();

            if (annualAccount != null)
            {
                //count the debt, payment and PurchaseCounter
                foreach (var acc in annualAccount)
                {
                    Payment_ += acc.Payment;
                    Debt_ += acc.Debt;
                    PurchasesCounter++;
                }
                
                Reports reports = new Reports
                {
                    Payment = Payment_,
                    Debt = Debt_,
                    AveragePurchases = Math.Round((decimal)PurchasesCounter / 365,2)
                };
                await databaseSave.SaveAnnualReports(reports);
                return reports;
            }
            return null;
        }

        public async Task<Reports> Selectmonthrpts()
        {
            DateTime dateTime = DateTime.Today;
            Payment_ = 0.0M;
            Debt_ = 0.0M;
            PurchasesCounter = 0;
            var date = DateTime.Today.AddMonths(-1);
            List<Account> monthlyAccounts = await crestfineContext.Accounts.Where(i => (i.Time >= date &&i.Time<=DateTime.Today)).ToListAsync();

            if (monthlyAccounts != null)
            {
                foreach (var monthlyacc in monthlyAccounts)
                {
                    Payment_ += monthlyacc.Payment;
                    Debt_ += monthlyacc.Debt;
                    PurchasesCounter++;
                }

                Reports reports = new Reports
                {
                    Payment = Payment_,
                    Debt = Debt_,
                    AveragePurchases = Math.Round((decimal)PurchasesCounter / 12,2)
                };
                await databaseSave.SaveMonthlyReports(reports);
                return reports;
            }
            return null;
        }

        public async Task<Reports> Selectweekrpts()
        {
            DateTime dateTime = DateTime.Today;
            var date = DateTime.Today.AddDays(-7);
            Payment_ = 0.0M;
            Debt_ = 0.0M;
            PurchasesCounter = 0;
            List<Account> weekAccount = await crestfineContext.Accounts.Where(i => (i.Time >= date && i.Time <= DateTime.Today)).ToListAsync();
            
            if(weekAccount != null)
            {
                foreach(var weekacc in weekAccount)
                {
                    Payment_ += weekacc.Payment;
                    Debt_ += weekacc.Debt;
                    PurchasesCounter++;
                }
                //decimal? avgpurch = (decimal?)PurchasesCounter / 7;
                Reports reports = new Reports
                {
                    Payment = Payment_,
                    Debt = Debt_,
                    AveragePurchases = Math.Round((decimal)PurchasesCounter / 7, 2)
            };
                await databaseSave.SaveWeekReports(reports);
                return reports;
            }
            return null;
        }
    }
}