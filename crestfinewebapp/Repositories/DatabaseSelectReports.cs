﻿using crestfinewebapp.Repositories.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfinewebapp.Repositories
{
    /// <summary>
    /// This Class returns reports fetched from Database
    /// </summary>
    public class DatabaseSelectReports : IDatabaseSelectReports
    {
        private readonly CrestfineContext crestfineContext;
        public DatabaseSelectReports()
        {
            crestfineContext = new CrestfineContext();
        }
        public async Task<AnnualReport> GetAnnualReport(int id)
        {
            var annualrpt = await crestfineContext.AnnualReports.Where(i => i.AnnualReportID == id).FirstOrDefaultAsync();
            return annualrpt;
        }

        public async Task<MonthlyReport> GetMonthReport(int id)
        {
            var monthlyrpt = await crestfineContext.MonthlyReports.Where(i => i.MonthlyReportID == id).FirstOrDefaultAsync();
            return monthlyrpt;
        }

        public async Task<WeekReport> GetWeekReport(int id)
        {
            var weekrpt = await crestfineContext.WeekReports.Where(i => i.ReportID == id).FirstOrDefaultAsync();
            return weekrpt;
        }

        public async Task<ICollection<AnnualReport>> GetAnnualReports()
        {
            List<AnnualReport> list = await crestfineContext.AnnualReports.ToListAsync();
            return list;
        }

        public async Task<ICollection<MonthlyReport>> GetMonthlyReports()
        {
            List<MonthlyReport> list = await crestfineContext.MonthlyReports.ToListAsync();
            return list;
        }
        
        public async Task<IEnumerable<WeekReport>> GetWeekReports()
        {
            IEnumerable<WeekReport> list = await crestfineContext.WeekReports.ToListAsync();
            return list;
        }
    }
}