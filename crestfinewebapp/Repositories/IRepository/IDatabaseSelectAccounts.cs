﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crestfinewebapp.Repositories.IRepository
{
    /// <summary>
    /// This interface implements select only on the database
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDatabaseSelectAccounts<T>
    {
        Task<T> Selectweekrpts();
        Task<T> Selectmonthrpts();
        Task<T> Selectannualrpts();
    }
}
