﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crestfinewebapp.Repositories.IRepository
{
    /// <summary>
    /// This interface fetches Reports
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDatabaseSelectReports
    {
        Task<IEnumerable<WeekReport>> GetWeekReports();
        Task<ICollection<MonthlyReport>> GetMonthlyReports();
        Task<ICollection<AnnualReport>> GetAnnualReports();

        Task<WeekReport> GetWeekReport(int id);
        Task<MonthlyReport> GetMonthReport(int id);
        Task<AnnualReport> GetAnnualReport(int id);
    }
}
