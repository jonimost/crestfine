﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfinewebapp.Repositories.IRepository
{
    public interface IDatabaseSave<T>
    {
        Task SaveWeekReports(T entity);
        Task SaveMonthlyReports(T entity);
        Task SaveAnnualReports(T entity);
    }
}