﻿using crestfinewebapp.Models;
using crestfinewebapp.Repositories.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfinewebapp.Repositories
{
    public class DatabaseSaveReports : IDatabaseSave<Reports>
    {
        
        private readonly CrestfineContext crestfineContext;
        public DatabaseSaveReports()
        {
            crestfineContext = new CrestfineContext();
        }

        public async Task SaveAnnualReports(Reports entity)
        {
            AnnualReport annualReports = new AnnualReport
            {
                Time = DateTime.Today,
                Amount = entity.Payment,
                Debt = entity.Debt,
                AverageAnnualSales = entity.AveragePurchases
            };
            crestfineContext.AnnualReports.Add(annualReports);
            await crestfineContext.SaveChangesAsync();
        }

        public async Task SaveMonthlyReports(Reports entity)
        {
            MonthlyReport monthlyReports = new MonthlyReport
            {
                Time = DateTime.Today,
                Amount = entity.Payment,
                Debt = entity.Debt,
                AverageMonthlySales = entity.AveragePurchases
            };
            crestfineContext.MonthlyReports.Add(monthlyReports);
            await crestfineContext.SaveChangesAsync();
        }

        public async Task SaveWeekReports(Reports entity)
        {
            WeekReport weekReports = new WeekReport
            {
                Time = DateTime.Today,
                Amount = entity.Payment,
                Debt = entity.Debt,
                AverageWeekSales = entity.AveragePurchases
            };
            crestfineContext.WeekReports.Add(weekReports);
            await crestfineContext.SaveChangesAsync();
        }
    }
}