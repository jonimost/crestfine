﻿using crestfinewebapp.DAL;
using crestfinewebapp.Models;
using crestfinewebapp.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace crestfinewebapp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel login_)
        {
           
            if (ModelState.IsValid)
            {
                var getTokenUrl = @"http://matothi.azurewebsites.net/token";

                using (HttpClient client = new HttpClient())
                {
                    HttpContent content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", login_.Email),
                        new KeyValuePair<string, string>("password", login_.Password)
                    });

                    
                    HttpResponseMessage httpResponse = client.PostAsync(getTokenUrl, content).Result;

                    string resultContent = httpResponse.Content.ReadAsStringAsync().Result;

                    var token = JsonConvert.DeserializeObject<Token>(resultContent);
                    if(token.access_token != null)
                    {
                        AuthenticationProperties options = new AuthenticationProperties
                        {
                            AllowRefresh = true,
                            IsPersistent = true,

                            ExpiresUtc = DateTime.UtcNow.AddSeconds(token.expires_in)
                        };

                        var claims = new[]
                        {
                        new Claim(ClaimTypes.Name,login_.Email),
                        new Claim("AccessToken", string.Format("Bearer {0}",token.access_token))
                    };

                        var identity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

                        Request.GetOwinContext().Authentication.SignIn(options, identity);
                        return RedirectToAction("Index", "WeekReport");
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Logout()
        {
            Request.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index");
        }
    }
}