﻿using crestfinewebapp.Models;
using crestfinewebapp.Repositories;
using crestfinewebapp.Repositories.IRepository;
using crestfinewebapp.ViewModels;
using Hangfire;
using Hangfire.Common;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList;
using crestfinewebapp.Attributes;

namespace crestfinewebapp.Controllers
{
    [WellbakeAuthorize]
    [CheckOutSession]
    public class AnnualReportController : Controller
    {
        private readonly IDatabaseSelectAccounts<Reports> databaseSelect;
        private readonly IDatabaseSelectReports annualreports;

        public AnnualReportController(IDatabaseSelectReports annualreports)
        {
            this.annualreports = annualreports;

            databaseSelect = new DatabaseSelectAccounts();
            
        }
        // GET: Annual
      
        //Autocheck annual for new Reports
        public async Task<ActionResult> AddReport()
        {
            var manage = new RecurringJobManager();
            manage.AddOrUpdate("annualreports", Job.FromExpression(() => databaseSelect.Selectannualrpts()), Cron.Yearly(1,1));
           
            ViewBag.Error = $"Reports as of {DateTime.Today}";
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DateSort = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            IEnumerable<AnnualReport> annualReport = await annualreports.GetAnnualReports();
            if (!String.IsNullOrEmpty(searchString))
            {
                annualReport = annualReport.Where(x => x.Time == Convert.ToDateTime(searchString));
            }

            switch (sortOrder)
            {
                case "Date":
                    annualReport = annualReport.OrderBy(s => s.Time);
                    break;
                case "date_desc":
                    annualReport = annualReport.OrderByDescending(s => s.Time);
                    break;
                default:
                    annualReport = annualReport.OrderBy(s => s.Time);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(annualReport.ToPagedList(pageNumber,pageSize));
        }

        public async Task<ActionResult> Details(int id)
        {
            AnnualReport annualReport = await annualreports.GetAnnualReport(id);
            
            return View(annualReport);
        }

        public async Task<FileResult> CreatePDF(int id)
        {
            //trigger the recurring tasks
            MemoryStream workstream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime time = DateTime.Today;

            string PDFfileName = string.Format("annualreport" + time.ToString("yyyyMMdd") + "-" + ".pdf");
            Document document = new Document();

            PdfPTable tableLayout = new PdfPTable(3);
            document.SetMargins(0f, 0f, 0f, 0f);
            //create pdf

            //Find the actual report
            AnnualReport annualrpt = await annualreports.GetAnnualReport(id);

            string strAttachment = Server.MapPath("~/Downloads/" + PDFfileName);

            PdfWriter.GetInstance(document, workstream).CloseStream = false;
            document.Open();

            document.Add(await AddContent(tableLayout, annualrpt));
            document.Close();

            byte[] byteInfo = workstream.ToArray();
            await workstream.WriteAsync(byteInfo, 0, byteInfo.Length);
            workstream.Position = 0;

            return File(workstream, "application/pdf", PDFfileName);
        }

        protected async Task<PdfPTable> AddContent(PdfPTable tableLayout, AnnualReport reports)
        {
            float[] headers = { 50, 24, 45};
            //Reports reports = await databaseSelect.Selectannualrpts();
            
            tableLayout.SetWidths(headers);
            tableLayout.WidthPercentage = 100;
            tableLayout.HeaderRows = 1;

            tableLayout.AddCell(new PdfPCell(new Phrase("WellBake Report", new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 5,
                HorizontalAlignment = Element.ALIGN_CENTER
            });

            AddCellToHeader(tableLayout, "Credit");
            AddCellToHeader(tableLayout, "Debit");
            AddCellToHeader(tableLayout, "AverageSales");

            //add body
            AddCellToBody(tableLayout, reports.Amount.ToString());
            AddCellToBody(tableLayout, reports.Debt.ToString());
            AddCellToBody(tableLayout, reports.AverageAnnualSales.ToString());


            tableLayout.AddCell(new PdfPCell(new Phrase("Annual Report" + " " + DateTime.Today, new Font(Font.FontFamily.HELVETICA, 8, 1, new iTextSharp.text.BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 5,
                HorizontalAlignment = Element.ALIGN_BOTTOM
            });

            return tableLayout;
        }

        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.YELLOW)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(128, 0, 0)
            });
        }

        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255)
            });
        }
    }
}