﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfinewebapp.Models
{ 
    /// <summary>
    /// Model for grabbing data from the database
    /// </summary>
    public class Reports
    {
        public decimal? Payment { get; set; }
        public decimal? Debt { get; set; }
        public decimal? AveragePurchases { get; set; }
    }
}