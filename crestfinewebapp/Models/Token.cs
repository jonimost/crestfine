﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfinewebapp.Models
{
    public class Token
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public Int16 expires_in { get; set; }
        public string roles { get; set; }
        public string issued { get; set; }
        public string expires { get; set; }
    }
}