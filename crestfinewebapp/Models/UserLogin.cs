﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfinewebapp.Models
{
    public class UserLogin: IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IDNumber { get; set; }
        public string Address { get; set; }
        public DateTime JoinDate { get; set; }
    }
}