namespace crestfinewebapp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WeekReport
    {
        [Key]
        public int ReportID { get; set; }
        [Key]
        public DateTime Time { get; set; }
        public decimal? Debt { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AverageWeekSales { get; set; }
    }
}
