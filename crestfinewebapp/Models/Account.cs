namespace crestfinewebapp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Account
    {
        public int AccountId { get; set; }

        public decimal? Payment { get; set; }

        public decimal? Debt { get; set; }

        public decimal? Balance { get; set; }

        public DateTime Time { get; set; }

        public int CustomerId { get; set; }

        public int PurchaseId { get; set; }
    }
}
