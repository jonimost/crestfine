﻿using crestfinewebapp.Repositories;
using crestfinewebapp.Repositories.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;

namespace crestfinewebapp.App_Start
{
    public class UnityConfig
    {
        public static void RegisterComponents(IUnityContainer container)
        {

        }
        public static IUnityContainer Config()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterType<IDatabaseSelectReports, DatabaseSelectReports>();
            RegisterComponents(container);
            return container;
        }

        public static IUnityContainer Initialize()
        {
            var container = Config();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            return container;
        }
    }
}