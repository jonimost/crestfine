﻿using System;
using System.Threading.Tasks;
using crestfinewebapp.DAL;
using Hangfire;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup("webappcrest",typeof(crestfinewebapp.App_Start.StartupConfig))]

namespace crestfinewebapp.App_Start
{
    public class StartupConfig
    {
        public void Configuration(IAppBuilder app)
        {
            //app.UseHangfireDashboard();
            //app.UseHangfireServer();

            //app.CreatePerOwinContext(() => new CrestfineContext());
            //app.CreatePerOwinContext<CrestfineManager>(CrestfineManager.Create);

            //app.CreatePerOwinContext<>
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Home/Index")
            });

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
        }
    }
}
