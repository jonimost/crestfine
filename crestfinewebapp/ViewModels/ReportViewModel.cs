﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfinewebapp.ViewModels
{
    public class ReportViewModel
    {
        public int ReportID { get; set; }

        public DateTime Time { get; set; }

        public decimal? Debt { get; set; }

        public decimal? Amount { get; set; }

        public decimal? AverageSales { get; set; }
    }
}