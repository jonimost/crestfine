﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using crestfine.Areas.Admin.Controllers;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace crestfine.Tests
{
    [TestClass]
    public class RouteTest
    {
        [TestMethod]
        public async Task GetAsyncRoute()
        {
            var mockrepo = new Mock<IRouteRepository>();
            mockrepo.Setup(x => x.GetById(1)).ReturnsAsync(route());

            var controller = new RouteController(mockrepo.Object);
            IHttpActionResult actionResult = await controller.Get(1);

            var contentResult = actionResult as OkNegotiatedContentResult<RouteModel>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.RouteId);
        }

        [TestMethod]
        public async Task GetAllAsyncRoutes()
        {
            var mockrepo = new Mock<IRouteRepository>();
            mockrepo.Setup(x => x.GetList()).ReturnsAsync(routes());
            var controller = new RouteController(mockrepo.Object);

            var result = await controller.Get() as List<RouteModel>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, routes().Count);
        }

        [TestMethod]
        public async Task PostRoute()
        {
            var mockrepo = new Mock<IRouteRepository>();
            var controller = new RouteController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Post(new Route { RouteId = 1, RouteName = "AP going Psycho", Details = "Post Malone" });
            var contentResult = actionResult as CreatedAtRouteNegotiatedContentResult<Route>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual("GetRoute", contentResult.RouteName);
            Assert.AreEqual(1, contentResult.RouteValues["id"]);
        }

        [TestMethod]
        public async Task PutRoute()
        {
            var mockrepo = new Mock<IRouteRepository>();
            var controller = new RouteController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new Route { RouteId = 1, RouteName = "Kiambu Town", Details = "Kirigiti-Kiambu" });
            var Contentresult = actionResult as NegotiatedContentResult<Route>;

            Assert.IsNotNull(Contentresult);
            Assert.IsNotNull(Contentresult.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, Contentresult.StatusCode);
            Assert.AreEqual(1, Contentresult.Content.RouteId);
        }

        [TestMethod]
        public async Task Delete()
        {
            var mockrepo = new Mock<IRouteRepository>();
            var controller = new RouteController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Delete(1);

            Assert.IsInstanceOfType(httpAction, typeof(OkResult));
        }


        public Route route()
        {
            var routeitem = new Route { RouteId = 1, RouteName = "Kiambu Town", Details = "Kirigiti-Kiambu" };

            return routeitem;
        }

        public List<Route> routes()
        {
            return new List<Route>(){
                new Route { RouteId = 1, RouteName = "Kiambu Town", Details = "Kirigiti-Kiambu" },
                new Route{RouteId=2, RouteName="Karatina", Details="Kirinyaga - Karatina"}
        };
        }
    }
}
