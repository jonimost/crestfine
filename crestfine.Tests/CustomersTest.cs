﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using crestfine.Areas.Admin.Controllers;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace crestfine.Tests
{
    [TestClass]
    public class CustomersTest
    {
        [TestMethod]
        public async Task GetAsyncCustomer()
        {
            var custmockrepo = new Mock<ICustomerRepository>();
            var purchmockrepo = new Mock<IPurchaseRepository>();
            var accmockrepo = new Mock<IAccountRepository>();
            custmockrepo.Setup(c => c.GetById(1)).ReturnsAsync(Customer());

            var controller = new CustomerController(custmockrepo.Object, accmockrepo.Object, purchmockrepo.Object);
            IHttpActionResult actionresult = await controller.Get(1);

            var result = actionresult as OkNegotiatedContentResult<CustomerModel>;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(1, result.Content.CustomerId);
        }

        [TestMethod]
        public async Task GetAsyncAllCustomer()
        {
            var custmockrepo = new Mock<ICustomerRepository>();
            var purchmockrepo = new Mock<IPurchaseRepository>();
            var accmockrepo = new Mock<IAccountRepository>();
            custmockrepo.Setup(c => c.GetList()).ReturnsAsync(Customers());

            var controller = new CustomerController(custmockrepo.Object, accmockrepo.Object, purchmockrepo.Object);
            var actionResult = await controller.Get() as List<CustomerModel>;

            Assert.IsNotNull(actionResult);
            Assert.AreEqual(actionResult.Count, Customers().Count);
        }

        [TestMethod]
        public async Task PostCustomer()
        {
            var custmockrepo = new Mock<ICustomerRepository>();
            var purchmockrepo = new Mock<IPurchaseRepository>();
            var accmockrepo = new Mock<IAccountRepository>();
            var controller = new CustomerController(custmockrepo.Object, accmockrepo.Object, purchmockrepo.Object);

            IHttpActionResult actionResult = await controller.Post(new Customers { CustomerId = 1, Name = "Customer", Email = "cust@gmail.com", Address = "Bloods", RouteId = 1, PhoneNumber = "15151212" });

            var contentResult = actionResult as CreatedAtRouteNegotiatedContentResult<Customers>;

            Assert.IsNotNull(actionResult);
            Assert.IsNotNull(contentResult);
            Assert.AreEqual("GetCustomer", contentResult.RouteName);
            Assert.AreEqual(1, contentResult.RouteValues["id"]);
        }

        [TestMethod]
        public async Task PutCustomer()
        {
            var custmockrepo = new Mock<ICustomerRepository>();
            var purchmockrepo = new Mock<IPurchaseRepository>();
            var accmockrepo = new Mock<IAccountRepository>();
            var controller = new CustomerController(custmockrepo.Object, accmockrepo.Object, purchmockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new Customers { CustomerId = 1, Address = "Atlanta Georgia", Name = "Migos", PhoneNumber = "+121591555", Email = "migos@brazzy.com", RouteId = 1 });

            var content = actionResult as NegotiatedContentResult<Customers>;

            Assert.IsNotNull(content);
            Assert.IsNotNull(content.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, content.StatusCode);
            Assert.AreEqual(1, content.Content.CustomerId);
        }

        [TestMethod]
        public async Task DeleteCustomer()
        {
            var custmockrepo = new Mock<ICustomerRepository>();
            var purchmockrepo = new Mock<IPurchaseRepository>();
            var accmockrepo = new Mock<IAccountRepository>();
            var controller = new CustomerController(custmockrepo.Object, accmockrepo.Object, purchmockrepo.Object);

            IHttpActionResult actionResult = await controller.Delete(1);
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        public Customers Customer()
        {
            var customer = new Customers { CustomerId = 1, PhoneNumber = "221515100", Email = "test@ku.ac.ke", Name = "Test", Address = "Kahawa", RouteId = 1 };
            return customer;
        }

        public List<Customers> Customers()
        {
            return new List<Customers>
            {
                new Customers{CustomerId=1, PhoneNumber="032644122", Name="Dae Dae", Email="Dae@live.com", Address="West Coast", RouteId=1},
                new Customers{CustomerId=2, PhoneNumber="932212111", Name="Tee Grizzey", Email="tgrizzey@gmail.com", Address="East Coast", RouteId=2}
            };
        }
    }
}
