﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using crestfine.Areas.Admin.Controllers;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace crestfine.Tests
{
    [TestClass]
    public class PurchaseTest
    {
        [TestMethod]
        public async Task GetAsyncPurchase()
        {
            var mockrepo = new Mock<IPurchaseRepository>();
            mockrepo.Setup(c => c.GetById(1)).ReturnsAsync(Purchase());
            var controller = new PurchasesController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Get(1);
            var result = httpAction as OkNegotiatedContentResult<PurchaseModel>;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(1, result.Content.PurchaseId);
        }

        [TestMethod]
        public async Task GetAsyncAllPurchases()
        {
            var mockrepo = new Mock<IPurchaseRepository>();
            mockrepo.Setup(c => c.GetList()).ReturnsAsync(Purchases());
            var controller = new PurchasesController(mockrepo.Object);

            var actionResult = await controller.Get() as List<PurchaseModel>;

            Assert.IsNotNull(actionResult);
            Assert.AreEqual(actionResult.Count, Purchases().Count);
        }

        [TestMethod]
        public async Task PostPurchase()
        {
            var mockrepo = new Mock<IPurchaseRepository>();
            var controller = new PurchasesController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Post(new List<Purchase> { new Purchase { PurchaseId = 1, CustomerId = 1, ProductId = 1, Quantity = 500, Payment = null, Amount = 0, IsPaid = false, Date = DateTime.Now, User = "Drake" } });

            Assert.IsNotNull(actionResult);
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public async Task PutPurchase()
        {
            var mockrepo = new Mock<IPurchaseRepository>();
            var controller = new PurchasesController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new Purchase { PurchaseId = 1, CustomerId = 1, ProductId = 1, Quantity = 500, Payment = null, Amount = 0, IsPaid = false, Date = DateTime.Now, User = "Drake" });
            var contentResult = actionResult as NegotiatedContentResult<Purchase>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.AreEqual(1, contentResult.Content.PurchaseId);
        }

        [TestMethod]
        public async Task DeletePurchase()
        {
            var mockrepo = new Mock<IPurchaseRepository>();
            var controller = new PurchasesController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Delete(1);
            Assert.IsInstanceOfType(httpAction, typeof(OkResult));
        }

        public Purchase Purchase()
        {
            var purch = new Purchase { PurchaseId = 1, CustomerId = 1, ProductId = 1, Quantity = 500, Payment = null, Amount = 0, IsPaid = false, Date = DateTime.Now, User = "Drake" };
            return purch;
        }

        public List<Purchase> Purchases()
        {
            return new List<Models.Purchase>
            {
                new Purchase { PurchaseId = 1, CustomerId = 1, ProductId = 1, Quantity = 500, Payment = null, Amount = 0, IsPaid = false, Date = DateTime.Now, User = "Drake" },
                new Purchase { PurchaseId = 1, CustomerId = 1, ProductId = 1, Quantity = 50, Payment = 700, Amount = 700, IsPaid = true, Date = DateTime.Now, User = "Future" }
            };
        }
    }
}
