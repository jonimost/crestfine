﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using crestfine.Areas.Admin.Controllers;
using crestfine.Models;
using crestfine.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace crestfine.Tests
{
    [TestClass]
    public class AssetsTest
    {
        
        [TestMethod]
        public async Task GetAsyncAssets_Ok()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            mockrepo.Setup(x => x.GetById(1)).ReturnsAsync(new Assets { AssetId = 1 });

            var controller = new AssetsController(mockrepo.Object);
            IHttpActionResult actionResult = await controller.Get(1);

            var contentresult = actionResult as OkNegotiatedContentResult<Assets>;

            Assert.IsNotNull(contentresult);
            Assert.IsNotNull(contentresult.Content);
            Assert.AreEqual(1, contentresult.Content.AssetId);
        }

        [TestMethod]
        public async Task GetAsyncAssets_NotFound()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            mockrepo.Setup(x => x.GetById(1)).ReturnsAsync((Assets)null);

            var controller = new AssetsController(mockrepo.Object);
            IHttpActionResult actionResult = await controller.Get(1);

            var contentResult = actionResult as NotFoundResult;

            Assert.IsNotNull(contentResult);
            
        }

        [TestMethod]
        public async Task GetAsyncAllAssets_Ok()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            mockrepo.Setup(x => x.GetList()).ReturnsAsync(Assetslist());

            var controller = new AssetsController(mockrepo.Object);
            var actionResult = await controller.Get() as List<Assets>;

            Assert.AreEqual(Assetslist().Count, actionResult.Count);
        }

        [TestMethod]
        public async Task PostAsset_Ok()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            var controller = new AssetsController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Post(new Assets { AssetId = 1, Item = "Truck", Description = "2008 Isuzu Truck: KBL 290P", Quantity = 1, Value = 1500000 });
            var createResult = actionResult as CreatedAtRouteNegotiatedContentResult<Assets>;


            Assert.IsNotNull(createResult);
            Assert.AreEqual("GetAsset", createResult.RouteName);
            Assert.AreEqual(1, createResult.RouteValues["id"]);
        }

        [TestMethod]
        public async Task PostAsset_BadRequest()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            var controller = new AssetsController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Post((Assets)null);
            var createresult = actionResult as BadRequestResult;

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestErrorMessageResult));
            Assert.IsNotNull(actionResult);
            Assert.IsNull(createresult);

        }

        [TestMethod]
        public async Task PutAsset_Ok()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            var controller = new AssetsController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new Assets { AssetId = 1, Item = "Stick", Description = "47 rounds", Quantity = 5, Value = 40000 });
            var contentResult = actionResult as NegotiatedContentResult<Assets>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.AreEqual(1, contentResult.Content.AssetId);

        }

        [TestMethod]
        public async Task PutAsset_BadRequest()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            var controller = new AssetsController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1,(Assets)null);
            var createresult = actionResult as BadRequestResult;

            Assert.IsInstanceOfType(actionResult, typeof(BadRequestErrorMessageResult));
            Assert.IsNotNull(actionResult);
            Assert.IsNull(createresult);

        }

        [TestMethod]
        public async Task DeleteAsset_Ok()
        {
            var mockrepo = new Mock<IAssetsRepository>();
            var controller = new AssetsController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Delete(1);

            Assert.IsInstanceOfType(httpAction, typeof(OkResult));
        }

        public Assets Assets()
        {
            var asst = new Assets { AssetId = 1, Item = "Truck", Description = "2008 Isuzu Truck: KBL 290P", Quantity = 1, Value = 1500000 };
            return asst;
        } 

        public List<Assets> Assetslist()
        {
            return new List<Assets>()
            {
                new Assets{AssetId=3, Item="Crates", Description="Bread Carrying Crates", Quantity=210, Value=600},
                new Assets{AssetId=4, Item="Ovens", Description="Used to Bake Bread", Quantity=2, Value=1700000}
            };
        }
    }
}
