﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using crestfine.Areas.Admin.Controllers;
using crestfine.Models;
using crestfine.Repositories;
using crestfine.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace crestfine.Tests
{
    [TestClass]
    public class LoadOrderTest
    {
        [TestMethod]
        public async Task GetAsyncAllLoadOrders()
        {
            var mockrepo = new Mock<ILoadingOrderRepository>();

            mockrepo.Setup(x => x.GetList()).ReturnsAsync(GetLoadingOrders());
            var controller = new LoadingOrderController(mockrepo.Object);
            var result = await controller.Get() as IDictionary<DateTime, List<ViewModels.LoadingOrdersViewModel>>;

            Assert.IsNotNull(result);
            Assert.AreEqual(GetLoadingOrders().Count, result.Count);
        }

        [TestMethod]
        public async Task GetAsyncLoadingOrder()
        {
            var mockrepo = new Mock<ILoadingOrderRepository>();
            mockrepo.Setup(x => x.GetById(1)).ReturnsAsync(new LoadingOrder { Id = 1 });

            var controller = new LoadingOrderController(mockrepo.Object);
            IHttpActionResult actionResult = await controller.Get(1);

            var contentresult = actionResult as OkNegotiatedContentResult<LoadingOrder>;

            Assert.IsNotNull(contentresult);
            Assert.IsNotNull(contentresult.Content);
            Assert.AreEqual(1, contentresult.Content.Id);
        }

        //[TestMethod]
        //public async Task PostAsset()
        //{
        //    var mockrepo = new Mock<ILoadingOrderRepository>();
        //    var controller = new LoadingOrderController(mockrepo.Object);

        //    IHttpActionResult actionResult = await controller.Post(new List<LoadingOrder>() { new LoadingOrder { Id = 1, LoadedQuantity = 250, LoadingDay = DateTime.Today.AddDays(-6), ProductId = 1, ReturnedQuantity = 50, RouteId = 1, SalesmanId = 1 } });

        //    Assert.IsNotNull(actionResult);
        //    Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        //}

        [TestMethod]
        public async Task PutAsset()
        {
            var mockrepo = new Mock<ILoadingOrderRepository>();
            var controller = new LoadingOrderController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new LoadingOrder { Id = 1, LoadedQuantity = 250, LoadingDay = DateTime.Today.AddDays(-6), ProductId = 1, ReturnedQuantity = 50,  SalesmanId = 1 });
            var contentResult = actionResult as NegotiatedContentResult<LoadingOrder>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.AreEqual(1, contentResult.Content.Id);

        }

        [TestMethod]
        public async Task DeleteAsset()
        {
            var mockrepo = new Mock<ILoadingOrderRepository>();
            var controller = new LoadingOrderController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Delete(1);

            Assert.IsInstanceOfType(httpAction, typeof(OkResult));
        }
        public LoadingOrder GetLoadingOrder()
        {
            var loadingOrder = new LoadingOrder { Id = 1, LoadedQuantity = 250, LoadingDay = DateTime.Today, ProductId = 1, ReturnedQuantity = 50, SalesmanId = 1 };
            return loadingOrder;
        }

        public IDictionary<DateTime,List<LoadingOrdersViewModel>> GetLoadingOrders()
        {
            Dictionary<DateTime, List<LoadingOrdersViewModel>> loadingOrders = new Dictionary<DateTime, List<LoadingOrdersViewModel>>();
            var orders =  new List<LoadingOrdersViewModel>
            {
                new LoadingOrdersViewModel { Id = 1, LoadingQuantity = 250, ReturnedQuantity = 50, Product="Viazi", Salesman="Kinuthia", Route="Kinang'op", User="Mwitwari"},
                new LoadingOrdersViewModel { Id = 2, LoadingQuantity = 1250, ReturnedQuantity = 850, Product="Kaimati", Salesman="Hassan", Route="Eastleign", User="Mwalimu"}
            };

            loadingOrders.Add(DateTime.Now, orders);
            return loadingOrders;
        }
    }
}
