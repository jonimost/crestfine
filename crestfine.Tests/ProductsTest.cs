﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using crestfine.Models;
using crestfine.Areas.Admin.Controllers;
using crestfine.Repositories;
using Moq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace crestfine.Tests
{
    [TestClass]
    public class ProductsTest
    {
        [TestMethod]
        public async Task GetAsyncProduct()
        {
            var mockrepo = new Mock<IProductsRepository>();

            mockrepo.Setup(x =>  x.GetById(5))
                .ReturnsAsync(new Products { ProductId = 5 });

            var controller = new ProductsController(mockrepo.Object);
            IHttpActionResult actionResult = await controller.Get(5);

            var contentResult = actionResult as OkNegotiatedContentResult<Products>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(5, contentResult.Content.ProductId);
        }

        [TestMethod]
        public async Task GetAsyncAllProducts()
        {
            ProductsRepository prodrepo = new ProductsRepository();
            var mockrepo = new Mock<IProductsRepository>();

            mockrepo.Setup(x => x.GetList())
                .ReturnsAsync(products());

            var controller = new ProductsController(mockrepo.Object);
            var actionResult = await controller.Get() as List<Products>;

            //assert
            Assert.AreEqual(products().Count, actionResult.Count);
        }

        [TestMethod]
        public async Task PostProduct()
        {
            var mockrepo = new Mock<IProductsRepository>();
            var controller = new ProductsController(mockrepo.Object);

            IHttpActionResult actionresult = await controller.Post(new Products { ProductId = 5, Name = "Kaimati", CrateSize = 55, Description = "Small Mandazi Packed in 20's", Cost = 35 });
            var createdResult = actionresult as CreatedAtRouteNegotiatedContentResult<Products>;

            Assert.IsNotNull(createdResult);
            Assert.AreEqual("GetProduct", createdResult.RouteName);
            Assert.AreEqual(5, createdResult.RouteValues["id"]);
        }

        [TestMethod]
        public async Task PutProduct()
        {
            var mockrepo = new Mock<IProductsRepository>();
            var controller = new ProductsController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new Products { ProductId = 1, Name = "Tee Grizzey", Cost = 400000000, CrateSize = 8100000, Description = "First Day Out" });
            var Contentresult = actionResult as NegotiatedContentResult<Products>;

            Assert.IsNotNull(Contentresult);
            Assert.IsNotNull(Contentresult.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, Contentresult.StatusCode);
            Assert.AreEqual(1, Contentresult.Content.ProductId);
        }

        [TestMethod]
        public async Task Delete()
        {
            var mockrepo = new Mock<IProductsRepository>();
            var controller = new ProductsController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Delete(1);

            Assert.IsInstanceOfType(httpAction, typeof(OkResult));
        }
        
        public Products product()
        {
            var tstprod = new Products { ProductId = 5, Name = "Kaimati_" };
            
            return tstprod;
        }

        public List<Products> products()
        {
            var tstprods = new List<Products>()
            {
                new Products{ProductId=5, Name="Kaandani", CrateSize=40, Cost=25},
                new Products{ProductId=6, Name="Aandazi", CrateSize=55, Cost=8},
                new Products{ProductId=7, Name="Sembe", CrateSize=80, Cost=7}
            };

            return tstprods;
        }
    }
}
