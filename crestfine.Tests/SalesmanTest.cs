﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using crestfine.Areas.Admin.Controllers;
using crestfine.Models;
using crestfine.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace crestfine.Tests
{
    [TestClass]
    public class SalesmanTest
    {
        [TestMethod]
        public async Task GetAsyncSalesman()
        {
            var mockrepo = new Mock<ISalesmanRepository>();
            mockrepo.Setup(x => x.GetById(1)).ReturnsAsync(new Salesman { SalesmanId = 1, Name = "Kaisari", IDNumber = 253610230, PhoneNumber = "+254727518227", RouteId = 1 });

            var controller = new SalesmanController(mockrepo.Object);
            IHttpActionResult actionResult = await controller.Get(1);

            var contentResult = actionResult as OkNegotiatedContentResult<Salesman>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(1, contentResult.Content.SalesmanId);
        }

        [TestMethod]
        public async Task GetAllAsyncSalesman()
        {
            var mockrepo = new Mock<ISalesmanRepository>();
            mockrepo.Setup(x => x.GetList()).ReturnsAsync(Salesman());

            var controller = new SalesmanController(mockrepo.Object);
            var actionResult = await controller.Get() as List<Salesman>;

            //assert 
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(actionResult.Count, Salesman().Count);
        }

        [TestMethod]
        public void PostSalesmanTest()
        {
            var mockrepo = new Mock<ISalesmanRepository>();
            var controller = new SalesmanController(mockrepo.Object);

            IHttpActionResult actionResult = controller.Post(new Salesman { SalesmanId = 1, Name = "Kaisari", IDNumber = 253610230, PhoneNumber = "+254727518227", RouteId = 1 });
            var contentResult = actionResult as CreatedAtRouteNegotiatedContentResult<Salesman>;

            Assert.IsNotNull(contentResult);
            Assert.AreEqual("GetSalesman", contentResult.RouteName);
            Assert.AreEqual(1, contentResult.RouteValues["id"]);
        }

        [TestMethod]
        public async Task PutSalesman()
        {
            var mockrepo = new Mock<ISalesmanRepository>();
            var controller = new SalesmanController(mockrepo.Object);

            IHttpActionResult actionResult = await controller.Put(1, new Salesman { SalesmanId = 1, Name = "Kaisari", IDNumber = 253610230, PhoneNumber = "+254727518227", RouteId = 1 });
            var Contentresult = actionResult as NegotiatedContentResult<Salesman>;

            Assert.IsNotNull(Contentresult);
            Assert.IsNotNull(Contentresult.Content);
            Assert.AreEqual(HttpStatusCode.Accepted, Contentresult.StatusCode);
            Assert.AreEqual(1, Contentresult.Content.SalesmanId);
        }

        [TestMethod]
        public async Task Delete()
        {
            var mockrepo = new Mock<ISalesmanRepository>();
            var controller = new SalesmanController(mockrepo.Object);

            IHttpActionResult httpAction = await controller.Delete(1);

            Assert.IsInstanceOfType(httpAction, typeof(OkResult));
        }


        public Salesman Saleman()
        {
            var salesman = new Salesman { SalesmanId = 1, Name = "Kaisari", IDNumber = 253610230, PhoneNumber = "+254727518227", RouteId = 1 };

            return salesman;
        }

        public List<Salesman> Salesman()
        {
            return new List<Salesman>()
            {
                new Salesman { SalesmanId = 1, Name = "Kaisari", IDNumber = 253610230, PhoneNumber = "+254727518227", RouteId = 1 },
                new Salesman { SalesmanId = 2, Name = "Kaisari Jr", IDNumber = 25165230, PhoneNumber = "+254720928227", RouteId = 2 }
            };
        }
    }
}
