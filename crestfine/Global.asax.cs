﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace crestfine
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            /*if (bool.Parse(ConfigurationManager.AppSettings["MigrateDatabaseToLatest"]))
            {
                var config = new crestfine.Migrations.Configuration();

                var migrator = new DbMigrator(config);
                migrator.Update();
            }*/
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
