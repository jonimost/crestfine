﻿using crestfine.Models;
using crestfine.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin,User")]
    [RoutePrefix("api/admin/assets")]
    public class AssetsController : ApiController
    {
        protected IAssetsRepository assetrepo;

        public AssetsController(IAssetsRepository asset)
        {
            assetrepo = asset;
        }

        // GET api/<controller>
        [Route("")]
        public async Task<IEnumerable<Assets>> Get()
        {
            var  assets = await assetrepo.GetList();
            return assets;
        }

        // GET api/<controller>/5
        [Route("{id:int}", Name ="GetAsset")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var ast = await assetrepo.GetById(id);

            if (ast == null)
            {
                return NotFound();
            }

            return Ok(ast);
        }

        // POST api/<controller>
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]Assets asset)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            else if (asset != null)
            {
                await assetrepo.Add(asset);

                return CreatedAtRoute(routeName: "GetAsset", routeValues: new { id = asset.AssetId }, content: asset);
            }
            return BadRequest("Asset is Null");
        }

        // PUT api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Assets asset)
        {
            //asset.AssetId = id;

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            else if(asset != null)
            {
                await assetrepo.Update(asset);
                return Content(HttpStatusCode.Accepted, asset);
            }

            return BadRequest("Asset is Null");
        }

        // DELETE api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            
            await assetrepo.Remove(id);
            return Ok();
        }
    }
}