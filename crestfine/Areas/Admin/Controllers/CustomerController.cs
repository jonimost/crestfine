﻿using AutoMapper;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using crestfine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin,User")]
    [RoutePrefix("api/admin/customers")]
    public class CustomerController : ApiController
    {
        private IPurchaseRepository purchrepo;
        private IAccountRepository accrepo;
        private ICustomerRepository custrepo;

        public CustomerController(ICustomerRepository custrp, IAccountRepository accrepo, IPurchaseRepository purchrepo)
        {
            custrepo = custrp;
            this.accrepo = accrepo;
            this.purchrepo = purchrepo;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Customers, CustomerModel>();
                cfg.CreateMap<Purchase, PurchaseModel>();
            });

            Mapper.AssertConfigurationIsValid();
        }

        // GET api/<controller>
        [Route("")]
        public async Task<IEnumerable<CustomerModel>> Get()
        {
            var customers = new CustomerViewModel();
            var cust = await custrepo.GetList();

            customers.Customers = Mapper.Map<IEnumerable<CustomerModel>>(cust);
            return customers.Customers;
        }

        // GET api/<controller>/5
        [Route("{id:int}", Name = "GetCustomer")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var customer = new CustomerViewModel();
            var cust = await custrepo.GetById(id);

            customer.Customer = Mapper.Map<CustomerModel>(cust);
            if (cust == null)
            {
                return NotFound();
            }

            
            return Ok(customer.Customer);
        }

        // POST api/<controller>
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]Customers customers)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            else if(customers != null)
            {

                await custrepo.Add(customers);
                return CreatedAtRoute(routeName: "GetCustomer", routeValues: new { id = customers.CustomerId }, content: customers);
            }

            return BadRequest("Customers is Null");
        }

        // PUT api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Customers customers)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            else if(customers != null)
            {
                await custrepo.Update(customers);
                return Content(HttpStatusCode.Accepted, customers);
            }
            return BadRequest("Customers is Null");
        }

        // DELETE api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            
            await custrepo.Remove(id);
            return Ok();
        }

        [Route("~/api/admin/customers/{id:int}/accountsummary")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAccountSummary(int id)
        {
            var accsummary = await accrepo.GetAccountSummary(id);

            if(accsummary != null)
            {
                return Ok(accsummary);
            }
            return NotFound();
        }

        [Route("~/api/admin/customers/{id:int}/unpaidpurchases")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUnpaidPurchases(int id)
        {

            var unpaidpurchases = await purchrepo.GetUnpaidPurchases(id);
            var purchases = new PurchasesViewModel();

            if(unpaidpurchases == null)
            {
                return NotFound();
            }
            purchases.PurchaseDictionary = Mapper.Map<IDictionary<DateTime, List<Purchase>>,IDictionary<DateTime, List<PurchaseModel>>>(unpaidpurchases);
            return Ok(purchases.PurchaseDictionary);
        }

        [Route("~/api/admin/customers/{id:int}/paidpurchases")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPaidPurchases(int id)
        {
            var paidpurchases = await purchrepo.GetPaidPurchases(id);
            var purchases = new PurchasesViewModel();
            if(paidpurchases == null)
            {
                return NotFound();
            }
            purchases.PurchaseDictionary = Mapper.Map<IDictionary<DateTime, List<Purchase>>, IDictionary<DateTime, List<PurchaseModel>>>(paidpurchases);

            return Ok(purchases.PurchaseDictionary);
        }

        [Route("~/api/admin/customers/{id:int}/purchases")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPurchases(int id)
        {
            var purchases = await purchrepo.GetPurchases(id);
            var purch = new PurchasesViewModel();
            if(purchases == null)
            {
                return NotFound();
            }

            purch.PurchaseDictionary = Mapper.Map<IDictionary<DateTime, List<Purchase>>, IDictionary<DateTime, List<PurchaseModel>>>(purchases);
            return Ok(purch.PurchaseDictionary);
        }

        [Route("~/api/admin/customers/{id:int}/purchases/{datetime:DateTime}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDatesPurchase(int id, DateTime dateTime)
        {
            var datesTime = await purchrepo.GetDatesPurchase(id, dateTime);
            var purchases = new PurchasesViewModel();
            if(datesTime == null)
            {
                return NotFound();
            }

            purchases.Purchases = Mapper.Map<IEnumerable<Purchase>, IEnumerable<PurchaseModel>>(datesTime);
            return Ok(purchases.Purchases);
        }

        [Route("~/api/admin/customers/{id:int}/payment")]
        [HttpPost]
        public async Task<IHttpActionResult> MakePayment(int id, [FromBody]List<Purchase> purchases)
        {
            DateTime UtcTime = DateTime.Today;
            DateTime LocalTime = TimeZoneInfo.ConvertTime(UtcTime, TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time"));
            foreach (var purch in purchases)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid Model State");
                }
                var bal = purch.Amount - purch.Payment;
                var accounts = new Accounts();
                accounts.Payment = purch.Payment;
                accounts.Time = LocalTime;
                accounts.PurchaseId = purch.PurchaseId;
                accounts.CustomerId = id;

                if(bal <= 0.00M)
                {
                    purch.IsPaid = true;
                }

                await accrepo.Add(accounts);
                await purchrepo.Update(purch);

            }
            return Ok();
        }


        //get list of accounts for a specified customer
        [Route("~/api/admin/customers/{id:int}/accounts")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAccounts(int id)
        {
            var accounts = new AccountViewModel();
            var acc = await accrepo.GetAccountsCustomerList(id);

            accounts.Accounts = Mapper.Map<IEnumerable<Accounts>>(acc);
            return Ok(accounts.Accounts);
        }
    }
}