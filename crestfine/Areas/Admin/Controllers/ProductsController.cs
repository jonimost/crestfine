﻿using crestfine.Models;
using crestfine.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin,User")]
    [RoutePrefix("api/admin/products")]
    public class ProductsController : ApiController
    {
        protected IProductsRepository prodrepo;
        protected List<Products> prod = new List<Products>();

        public ProductsController(IProductsRepository productsrepo)
        {
            prodrepo = productsrepo;
        }

        // GET api/<controller>
        [Route("")]
        public async Task<IEnumerable<Products>> Get()
        {
            return await prodrepo.GetList();
        }

        // GET api/<controller>/5
        [Route("{id:int}", Name ="GetProduct")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var prod = await prodrepo.GetById(id);

            if(prod == null)
            {
                return NotFound();
            }

            return Ok(prod);
        }

        // POST api/<controller>
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]Products product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            await prodrepo.Add(product);

            return CreatedAtRoute(routeName: "GetProduct", routeValues: new { id = product.ProductId }, content: product);
        }

        // PUT api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Products product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }

            await prodrepo.Update(product);
            return Content(HttpStatusCode.Accepted, product);
        }

        // DELETE api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await prodrepo.Remove(id);
            return Ok();
        }
    }
}