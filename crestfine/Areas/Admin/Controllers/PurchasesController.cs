﻿using AutoMapper;
using crestfine.DAL;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using crestfine.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin,User")]
    [RoutePrefix("api/admin/purchases")]
    public class PurchasesController : ApiController
    {
        private IAccountRepository accrepo;
        private IPurchaseRepository purchrepo;
        private ICustomerRepository customerepo;
        private IProductsRepository prodrepo;
        private ILoadingOrderRepository loadingOrder;

        //public PurchasesController(
        //                        IAccountRepository accrepo,
        //                        ICustomerRepository customerepo,
        //                        IProductsRepository prodrepo,
        //                        ILoadingOrderRepository loadingOrder)
        //{

            
        //}

        public PurchasesController(IPurchaseRepository purchaserepo)
        {
            purchrepo = purchaserepo;
            this.accrepo = new AccountRepository();
            this.customerepo = new CustomerRepository();
            this.prodrepo = new ProductsRepository();
            this.loadingOrder = new LoadingOrderRepository();

            Mapper.Initialize(cfg => cfg.CreateMap<Purchase, PurchaseModel>());
            Mapper.AssertConfigurationIsValid();
        }
        // GET api/<controller>
        [Route("")]
        [HttpGet]
        public async Task<IEnumerable<PurchaseModel>> Get()
        {
            var purchase = new PurchasesViewModel();
            var purch = await purchrepo.GetList();

            purchase.Purchases = Mapper.Map<IEnumerable<PurchaseModel>>(purch);
            return purchase.Purchases;
        }

        // GET api/<controller>/5
        [Route("{id:int}", Name ="GetPurchase")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            var purch = await purchrepo.GetById(id);
            var purchase = new PurchasesViewModel();
            if(purch == null)
            {
                return NotFound();
            }

            purchase.Purchase = Mapper.Map<PurchaseModel>(purch);
            return Ok(purchase.Purchase);
        }

        // POST api/<controller>
        [Route("")]
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]List<Purchase> purchases)
        {
            
            var manager = new UserManager<User>(new UserStore<User>(new CrestfineContext()));

            var user = await manager.FindByNameAsync(RequestContext.Principal.Identity.Name);
            DateTime UtcTime = DateTime.Today;
           //DateTime LocalTime = TimeZoneInfo.ConvertTime(UtcTime, TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time"));
            foreach (var purch in purchases)
            {

               var product = await prodrepo.GetById(purch.ProductId);
               purch.Date = UtcTime;
               purch.Amount = purch.Quantity * product.Cost;
               purch.User = String.Format($"{user.FirstName} {user.LastName}");
               await purchrepo.Add(purch);
            }

            //add new loading order.
            foreach (var purch in purchases)
            {
                var loadingOrder_ = new Models.LoadingOrder();
                loadingOrder_.LoadedQuantity = purch.Quantity;
                loadingOrder_.ProductId = purch.ProductId;
                loadingOrder_.SalesmanId = purch.SalesmanId;
                loadingOrder_.LoadingDay = UtcTime;
                loadingOrder_.User = String.Format($"{user.FirstName} {user.LastName}");

                if (!await this.loadingOrder.IsLoaded(UtcTime, loadingOrder_.ProductId))
                {
                    await this.loadingOrder.Add(loadingOrder_);
                }
                else
                {
                    var load = await this.loadingOrder.GetLoadingOrder(loadingOrder_);
                    load.LoadedQuantity += loadingOrder_.LoadedQuantity;
                    
                    var result = await this.loadingOrder.Update(load);
                }

            }


            return Ok();
        }

        // PUT api/<controller>/5
        [Route("{id:int}")]
        [HttpPut]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Purchase purchase)
        {
            var manager = new UserManager<User>(new UserStore<User>(new CrestfineContext()));

            var user = await manager.FindByNameAsync(RequestContext.Principal.Identity.Name);
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            var bal = purchase.Amount - purchase.Payment;
            if (bal <= 0.00M)
            {
                purchase.IsPaid = true;
            }

            purchase.User = String.Format($"{user.FirstName} {user.LastName}");
            await purchrepo.Update(purchase);
            return Content(HttpStatusCode.Accepted, purchase);
        }

        // DELETE api/<controller>/5
        [Route("{id:int}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await purchrepo.Remove(id);
            return Ok();
        }

        [Route("~/api/admin/purchases/pastdeadline")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPurchasesPastDeadline()
        {
            var unpaidpurchases = await purchrepo.GetPurchasesPastDeadline();
            var purchases = new PurchasesViewModel();
            if (unpaidpurchases == null)
            {
                return NotFound();
            }

            purchases.PurchaseDictionary = Mapper.Map<IDictionary<DateTime, List<Purchase>>, IDictionary<DateTime, List<PurchaseModel>>>(unpaidpurchases);
            return Ok(purchases.PurchaseDictionary);
        }

        [Route("~/api/admin/purchases/dayspurchase/{dateTime:DateTime}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDaysPurchases(DateTime dateTime)
        {
            var dayspurchases =await purchrepo.GetDaysPurchases(dateTime);
            var purchases = new PurchasesViewModel();
            
            purchases.DayPurchase= Mapper.Map<IDictionary<string, List<Purchase>>,IDictionary<string, List<PurchaseModel>>>(dayspurchases);
            
            return Ok(purchases.DayPurchase);
        }
    }
}