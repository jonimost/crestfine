﻿using crestfine.DAL;
using crestfine.Models;
using crestfine.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    [RoutePrefix("api/admin/salesman")]
    public class SalesmanController : ApiController
    {
        protected ISalesmanRepository salemanrep;
    

        public SalesmanController(ISalesmanRepository salesmanrepo)
        {
            salemanrep = salesmanrepo;

        }

        // GET api/<controller>
        [Route("",Name ="GetList")]
        public async Task<IEnumerable<Salesman>> Get()
        {
            return await salemanrep.GetList();
        }

        // GET api/<controller>/5
        [Route("{id:int}", Name ="GetSalesman")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var salemn = await salemanrep.GetById(id);

            if(salemn == null)
            {
                return NotFound();
            }
            return Ok(salemn);
        }

        // POST api/<controller>
        [Route("")]
        public IHttpActionResult Post([FromBody]Salesman saleman)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }
            salemanrep.Add(saleman);
            return CreatedAtRoute(routeName: "GetSalesman", routeValues: new { id = saleman.SalesmanId }, content: saleman);
        }

        // PUT api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Salesman saleman)
        {
            saleman.SalesmanId = id;

          
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }

            await salemanrep.Update(saleman);
            return Content(HttpStatusCode.Accepted, saleman);
        }

        // DELETE api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await salemanrep.Remove(id);
            return Ok();
        }
    }
}