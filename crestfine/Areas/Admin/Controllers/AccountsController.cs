﻿using AutoMapper;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using crestfine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin,User")]
    [RoutePrefix("api/admin/accounts")]
    public class AccountsController : ApiController
    {
        IAccountRepository accountsrepo;

        public AccountsController(IAccountRepository accrepo)
        {
            accountsrepo = accrepo;
            Mapper.Initialize(cfg => cfg.CreateMap<AccountModel, Accounts>());
            Mapper.AssertConfigurationIsValid();
        }

        // GET: api/Accounts
        [Route("")]
        public async Task<IEnumerable<Accounts>> Get()
        {
            var accounts = new AccountViewModel();
            var acc_ = await accountsrepo.GetList();

            accounts.Accounts = Mapper.Map<IEnumerable<Accounts>>(acc_);
            return accounts.Accounts;
        }

        // GET: api/Accounts/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var acc_ = await accountsrepo.GetById(id);
            var accounts = new AccountViewModel();
            if(acc_ == null)
            {
                return NotFound();
            }
            accounts.Account = Mapper.Map<Accounts>(acc_);
            return Ok(accounts.Account);
        }
        
    }
}
