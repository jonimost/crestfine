﻿using AutoMapper;
using crestfine.DAL;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using crestfine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin,User")]
    [RoutePrefix("api/admin/routes")]
    public class RouteController : ApiController
    {
        protected IRouteRepository routerepo;

        public RouteController(IRouteRepository routerp)
        {
            routerepo = routerp;
            Mapper.Initialize(cfg => cfg.CreateMap<Route, RouteModel>());
            Mapper.AssertConfigurationIsValid();
        }


        // GET api/<controller>
        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            var routeres = new RouteViewModel();
            var routes = await routerepo.GetList();
            routeres.Routes = Mapper.Map<IEnumerable<RouteModel>>(routes);
            return Ok(routeres.Routes);

        }

        // GET api/<controller>/5
        [Route("{id:int}", Name ="GetRoute")]
        public async Task<IHttpActionResult> Get(int id)
        {
            var routeres = new RouteViewModel();
            var route_ = await routerepo.GetById(id);

            if(route_ == null)
            {
                return NotFound();
            }

            routeres.Route = Mapper.Map<RouteModel>(route_);
            return Ok(routeres.Route);
        }

        // POST api/<controller>
        [Route("")]
        public async Task<IHttpActionResult> Post([FromBody]Route route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }

            await routerepo.Add(route);
            return CreatedAtRoute(routeName: "GetRoute", routeValues: new { id = route.RouteId }, content: route);
        }

        // PUT api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Route route)
        {
            route.RouteId = id;

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }

            await routerepo.Update(route);
            return Content(HttpStatusCode.Accepted, route);
        }

        // DELETE api/<controller>/5
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await routerepo.Remove(id);
            return Ok();
        }
    }
}