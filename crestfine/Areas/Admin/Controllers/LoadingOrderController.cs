﻿using AutoMapper;
using crestfine.DAL;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.Repositories;
using crestfine.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Areas.Admin.Controllers
{
    [RoutePrefix("api/admin/loadingorder")]
    [Authorize(Roles ="Admin, User")]
    public class LoadingOrderController : ApiController
    {
        private ILoadingOrderRepository loadingOrder;

        public LoadingOrderController(ILoadingOrderRepository loadingOrder)
        {
            this.loadingOrder = loadingOrder;
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Models.LoadingOrder, LoadingOrderModel>();
                cfg.CreateMap<Models.LoadingOrder, LoadingOrdersViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Id))
                .ForMember(dest => dest.LoadingQuantity, opt => opt.MapFrom(x => x.LoadedQuantity))
                .ForMember(dest => dest.ReturnedQuantity, opt => opt.MapFrom(x => x.ReturnedQuantity))
                .ForMember(dest => dest.Product, opt => opt.MapFrom(x => x.Products.Name))
                .ForMember(dest => dest.Salesman, opt => opt.MapFrom(x => x.Salesman.Name))
                .ForMember(dest => dest.User, opt => opt.MapFrom(x => x.User));
            });
            Mapper.AssertConfigurationIsValid();
        }

        // GET api/<controller>
        [Route("")]
        [HttpGet]
        public async Task<IDictionary<DateTime, List<LoadingOrdersViewModel>>> Get()
        {
            var load = await this.loadingOrder.GetList();
            var loadview = Mapper.Map</*IDictionary<DateTime, List<Models.LoadingOrder>>,*/IDictionary<DateTime, List<LoadingOrdersViewModel>>>(load);
            return loadview;
        }

        // GET api/<controller>/5
        [Route("{id:int}", Name ="GetLoad")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(int id)
        {
            var load = await this.loadingOrder.GetById(id);

            if(load == null)
            {
                return NotFound();
            }
            var loadingViewModel = new LoadingOrderViewModel();

            loadingViewModel.LoadingOrderModel = Mapper.Map<Models.LoadingOrder, LoadingOrderModel>(load);
            return Ok(load);
        }

        
        // PUT api/<controller>/5
        [HttpPut]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Put(int id, [FromBody]Models.LoadingOrder value)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid Model State");
            }

            await this.loadingOrder.Update(value);
            return Content(HttpStatusCode.Accepted, value);
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        [Route("{id:int}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await this.loadingOrder.Remove(id);
            return Ok();
        }
    }
}