﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using Microsoft.Practices.Unity;
using Unity;
using crestfine.Repositories;
using Unity.Lifetime;
using crestfine.Middleware;
using crestfine.Models;
using crestfine.DAL;
using Unity.Injection;
using crestfine.Areas.Admin.Controllers;
using crestfine.Controllers;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Cors;

namespace crestfine
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Web API routes
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            UnityConfiguration unity = new UnityConfiguration();
            var container = unity.Config();

            config.DependencyResolver = new UnityResolver(container);

            //use only Token Auth
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.Formatters.JsonFormatter
                            .SerializerSettings
                            .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;/**/

            /*config.Formatters.JsonFormatter
                            .SerializerSettings
                            .PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;*/
            config.Formatters.JsonFormatter.
                               SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

        }
    }

    public class UnityConfiguration
    {
        public IUnityContainer Config()
        {
            IUnityContainer container = new UnityContainer();

            //register Controllers
            //admin
            container.RegisterType<AssetsController>();
            container.RegisterType<CustomerController>();
            container.RegisterType<ProductsController>();
            container.RegisterType<PurchasesController>();
            container.RegisterType<RouteController>();
            container.RegisterType<SalesmanController>();
            

            //register Interfaces
            container.RegisterType<IAssetsRepository, AssetsRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICustomerRepository, CustomerRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IProductsRepository, ProductsRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ISalesmanRepository, SalesmanRespository>(new HierarchicalLifetimeManager());
            container.RegisterType<IRouteRepository, RouteRespository>(new HierarchicalLifetimeManager());
            container.RegisterType<IAccountRepository, AccountRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IPurchaseRepository, PurchaseRespository>(new HierarchicalLifetimeManager());
            container.RegisterType<ILoadingOrderRepository, LoadingOrderRepository>(new HierarchicalLifetimeManager());

            return container;
        }
    }
}
