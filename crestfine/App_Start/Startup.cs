﻿using crestfine.DAL;
using crestfine.Models;
using crestfine.Providers;
using Hangfire;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

[assembly: OwinStartup("crestfine",typeof(crestfine.App_Start.Startup))]
namespace crestfine.App_Start
{
    public class Startup
    {
        private static OAuthAuthorizationServerOptions OAuthOptions { get; set; }
        private static string PublicClientIdentity { get; set; }

        public void Configuration(IAppBuilder app)
        {
            
            HttpConfiguration config = new HttpConfiguration();
            ConfigureOAuth(app);

            

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            Hangfire.GlobalConfiguration.Configuration
                .UseSqlServerStorage("WelbeckDatabase");
            //app.UseHangfireDashboard();
            //app.UseHangfireServer();
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext<CrestfineContext>(CrestfineContext.Create);
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.CreatePerOwinContext<AppRoleManager>(AppRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());


            PublicClientIdentity = "self";
            OAuthAuthorizationServerOptions OAuthserveroptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(2),
                Provider = new AuthorizationServerProvider()
            };
            app.UseOAuthAuthorizationServer(OAuthserveroptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}