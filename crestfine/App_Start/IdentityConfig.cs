﻿using crestfine.DAL;
using crestfine.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace crestfine.App_Start
{
    public class AppUserManager : UserManager<User, string>
    {
        public AppUserManager(IUserStore<User, string> store)
            :base(store)
        {

        }

        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var dbcontext = context.Get<CrestfineContext>();
            /*var manager = new AppUserManager(
                            new UserStore<User, IdentityRole, string, AppUserLogin, AppUserRole,
                            AppUserClaim>(context.Get<CrestfineContext>()));
            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6
            };
            var dataprotectionprovider = options.DataProtectionProvider;
            if (dataprotectionprovider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<User>(dataprotectionprovider.Create("Wellbake Identity"));
            }*/

           
            var manager = new AppUserManager(new UserStore<User>(dbcontext));
            
             return manager;
        }

    }

    public class AppRoleManager : RoleManager<IdentityRole>
    {
        public AppRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static AppRoleManager Create(
            IdentityFactoryOptions<AppRoleManager> options,
            IOwinContext context)
        {
            return new AppRoleManager(
                new RoleStore<IdentityRole>(context.Get<CrestfineContext>()));
        }
    }
    
}