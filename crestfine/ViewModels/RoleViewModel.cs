﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    public class RoleViewModel
    {
        [Required]
        [StringLength(25)]
        public string Name { get; set; }
    }

    public class UsersInRoleModel
    {
        public string Id { get; set; }
        public IList<string> Users { get; set; }
    }

    public class RemoveUserFromRole
    {
        public List<string> Users { get; set; }
    }
}