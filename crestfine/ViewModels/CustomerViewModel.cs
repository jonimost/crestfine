﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    public class CustomerViewModel
    {
        public IEnumerable<CustomerModel> Customers { get; set; }
        public CustomerModel Customer { get; set; }
    }
}