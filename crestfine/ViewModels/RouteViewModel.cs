﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    public class RouteViewModel
    {

        public RouteModel Route { get; set; }
        public IEnumerable<RouteModel> Routes { get; set; }
    }
}