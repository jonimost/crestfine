﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    [Serializable]
    public class LoadingOrdersViewModel
    {
      public int Id { get; set; }
      public int LoadingQuantity { get; set; }
      public int? ReturnedQuantity { get; set; }
      public string Product { get; set; }
      public string Salesman { get; set; }
      //public string Route { get; set; }
      public string User { get; set; }
    }
}