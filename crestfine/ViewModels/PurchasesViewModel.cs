﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    public class PurchasesViewModel
    {
        public IEnumerable<PurchaseModel> Purchases { get; set; }
        public PurchaseModel Purchase { get; set; }
        public IDictionary<DateTime, List<PurchaseModel>> PurchaseDictionary { get; set; }
        public IDictionary<string, List<PurchaseModel>> DayPurchase { get; set; }
    }
}