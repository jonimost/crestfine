﻿using crestfine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    public class RouteCustomerViewModel
    {
        public Route Route { get; set; }
        public string Name { get; set; }
        public int PhoneNumber { get; set; }
    }
}