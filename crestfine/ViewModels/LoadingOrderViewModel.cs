﻿using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.ViewModels
{
    public class LoadingOrderViewModel
    {
        public LoadingOrderModel LoadingOrderModel { get; set; }
        public IDictionary<DateTime,List<LoadingOrderModel>> Loadingdictionary { get; set; }
    }
}