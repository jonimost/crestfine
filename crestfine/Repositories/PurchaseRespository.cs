﻿using AutoMapper;
using crestfine.DAL;
using crestfine.Models;
using crestfine.Models.DTO;
using crestfine.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{

    public class PurchaseRespository: IPurchaseRepository
    {
        private CrestfineContext cstentity;
        private AccountRepository accrepo = new AccountRepository();

        public PurchaseRespository()
        {
            this.cstentity = new CrestfineContext();
          
        }

        public async Task<IEnumerable<Purchase>> GetList() => await cstentity.Purchase.OrderByDescending(x => x.Date).ToListAsync();
                     

        public async Task<Purchase> GetById(int id_) => await
            cstentity.Purchase.Where(purch => purch.PurchaseId == id_)
                    .FirstOrDefaultAsync();

        public async Task Add(Purchase purchase_)
        {
            Accounts accounts = new Accounts();

            //add accounts simultaneously
            accounts.Time = purchase_.Date;
            accounts.CustomerId = purchase_.CustomerId;
            accounts.Debt = purchase_.Amount;
            
            cstentity.Purchase.Add(purchase_);
            cstentity.SaveChanges();
           

            accounts.PurchaseId = cstentity.Purchase.Where(id => id.PurchaseId == purchase_.PurchaseId)
                                        .Select(x=> x.PurchaseId).FirstOrDefault();
            cstentity.Accounts.Add(accounts);
            await cstentity.SaveChangesAsync();
        }

        public async Task Remove(int id_)
        {
            var purch = cstentity.Purchase.Where(prchase => prchase.PurchaseId == id_).FirstOrDefault();
            var account =cstentity.Accounts.Where(acc => acc.PurchaseId == id_).FirstOrDefault();
            cstentity.Purchase.Remove(purch);
            await accrepo.Remove(account.AccountId);
            await cstentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Purchase entity)
        {
            var purch = await cstentity.Purchase.Where(pur => pur.PurchaseId == entity.PurchaseId).FirstOrDefaultAsync();
            var acc = await cstentity.Accounts.Where(pur => pur.PurchaseId == entity.PurchaseId).FirstOrDefaultAsync();
            if(purch == null || acc == null)
            {
                return false;
            }
            //update
            acc.Payment = entity.Payment;
            acc.Debt = entity.Amount;

            cstentity.Entry(purch).CurrentValues.SetValues(entity);
            cstentity.SaveChanges();
            await accrepo.Update(acc);
            return true;
        }

        public async Task<IDictionary<DateTime,List<Purchase>>> GetUnpaidPurchases(int id)
        {
            var unpaidpurchases = await cstentity.Purchase.Where(pur => pur.CustomerId == id && pur.IsPaid == false).GroupBy(i => i.Date).ToDictionaryAsync(p => p.Key, x => x.Select(i => i).ToList());

            return unpaidpurchases;
        }

        public async Task<IDictionary<DateTime, List<Purchase>>> GetPaidPurchases(int id)
        {
            var paidpurchases = await cstentity.Purchase.Where(pur => pur.CustomerId == id && pur.IsPaid == true).GroupBy(i => i.Date).ToDictionaryAsync(pur => pur.Key, pur => pur.Select(i => i).ToList());

            return new SortedDictionary<DateTime, List<Purchase>>(paidpurchases);
        }
        
        public async Task<IList<Purchase>> GetDatesPurchase(int id, DateTime dateTime)
        {
            var datespurchase = await cstentity.Purchase.Where(pur => pur.CustomerId == id && pur.Date == dateTime).ToListAsync();

            return datespurchase;
        }

        public async Task<IDictionary<DateTime, List<Purchase>>> GetPurchases(int id)
        {
            SortedDictionary<DateTime, List<Purchase>> sortedDictionary = new SortedDictionary<DateTime, List<Purchase>>();
            var purchases = await cstentity.Purchase.Where(pur => pur.CustomerId == id).GroupBy(i => i.Date)
                            .ToDictionaryAsync(dt=>dt.Key, dt => dt.Select(i => i).ToList());
            
            return new SortedDictionary<DateTime, List<Purchase>>(purchases);
            
        }

        public async Task<IDictionary<DateTime,List<Purchase>>> GetPurchasesPastDeadline()
        {
            var date = DateTime.Today.AddDays(-3);
            var pastdeadline = await cstentity.Purchase.Where(p => (p.IsPaid == false) && (date >=p.Date)).GroupBy(i => i.Date).ToDictionaryAsync(pur => pur.Key, pur => pur.Select(i => i).ToList());
            
            return new SortedDictionary<DateTime, List<Purchase>>(pastdeadline);
        }
           


        public async Task<IDictionary<string, List<Purchase>>> GetDaysPurchases(DateTime dateTime)
        {
            var dayspurchases = await cstentity.Purchase.Where(p => p.Date == dateTime).GroupBy(i => i.Customer.Name).ToDictionaryAsync(pur => pur.Key, pur => pur.Select(i => i).ToList());

            return new SortedDictionary<string, List<Purchase>>(dayspurchases);
        }
        

        Task<Purchase> IRepository<Purchase>.GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<decimal> AverageWeeklyPurchases()
        {
            decimal avgweekpurchases = await cstentity.Purchase.Where(i => i.Date - DateTime.Today <= TimeSpan.FromDays(7)).CountAsync() / 7;
            return avgweekpurchases;
        }

        public async Task<decimal> AverageMonthlyPurchases()
        {
            decimal avgmonthlypurchases = await cstentity.Purchase.Where(i => i.Date - DateTime.Today <= TimeSpan.FromDays(28)).CountAsync() / 28;
            return avgmonthlypurchases;
        }

        public async Task<decimal> AverageYearlyPurchases()
        {
            decimal avgyearlypurchases = await cstentity.Purchase.Where(i => i.Date - DateTime.Today <= TimeSpan.FromDays(365)).CountAsync() / 365;
            return avgyearlypurchases;
        }
    }
}