﻿using crestfine.DAL;
using crestfine.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public class AssetsRepository: IAssetsRepository
    {
        private CrestfineContext cstentity;

        public AssetsRepository()
        {
            this.cstentity = new CrestfineContext();
        }

        public async Task<IEnumerable<Assets>> GetList()
        {
            var assetlist = await cstentity.Assets.ToListAsync();
            return assetlist;
        }

        public async Task<Assets> GetById(int id_)
        {
            var asset = await cstentity.Assets.Where(ast => ast.AssetId == id_).FirstOrDefaultAsync();
            return asset;
        }

        public async Task Add(Assets asset_)
        {
            cstentity.Assets.Add(asset_);
            await cstentity.SaveChangesAsync();
        }

        public async Task Remove(int id_)
        {
            var asset_ = cstentity.Assets.Where(asst => asst.AssetId == id_).FirstOrDefault();
            cstentity.Assets.Remove(asset_);
            await cstentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Assets entity)
        {
            var ast = cstentity.Assets.Where(a => a.AssetId == entity.AssetId).FirstOrDefault();

            if(ast == null)
            {
                return false;
            }

            cstentity.Entry(ast).CurrentValues.SetValues(entity);
            await cstentity.SaveChangesAsync();
            return true;
        }
    }
}