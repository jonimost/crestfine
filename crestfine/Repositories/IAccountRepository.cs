﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface IAccountRepository: IRepository<Accounts>
    {
        Task<IEnumerable<Accounts>> GetList();
        Task<IEnumerable<Accounts>> GetAccountsCustomerList(int id);
        Task<decimal?> GetWeekPayment();
        Task<decimal?> GetWeekDebt();
        Task<decimal?> GetMonthPayment();
        Task<decimal?> GetMonthDebt();
        Task<decimal?> GetAnnualPayment();
        Task<decimal?> GetAnnualDebt();
        Task<decimal?> GetPayment(int id);
        Task<decimal?> GetDebt(int id);
        Task<AccountSummaryModel> GetAccountSummary(int id);
        void AddDebt(int id);
        void Balance(int id);

    }
}