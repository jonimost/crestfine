﻿using crestfine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface IAnnualReportRepository: IRepository<AnnualReports>
    {
        Task<IEnumerable<AnnualReports>> GetList();
    }
}