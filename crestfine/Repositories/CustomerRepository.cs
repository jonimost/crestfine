﻿using crestfine.DAL;
using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public class CustomerRepository: ICustomerRepository
    {
        private CrestfineContext cstentity;

        public CustomerRepository()
        {
            this.cstentity = new CrestfineContext();
        }

        public async Task<IEnumerable<Customers>> GetList()
        {
            var customerlist = await cstentity.Customers.OrderByDescending(cust => cust.Name).ToListAsync();
            return customerlist;
        }

        public async Task<Customers> GetById(int id_)
        {
            var customer = await cstentity.Customers.Where(cst => cst.CustomerId == id_)
                                    .FirstOrDefaultAsync();
            return customer;
        }

        public async Task Add(Customers cst)
        {
            cstentity.Customers.Add(cst);
            await cstentity.SaveChangesAsync();
        }

        public async Task Remove(int id_)
        {
            var customer = cstentity.Customers.Where(cst => cst.CustomerId == id_).FirstOrDefault();
            cstentity.Customers.Remove(customer);
            await cstentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Customers entity)
        {
            var cust = cstentity.Customers.Where(cst => cst.CustomerId == entity.CustomerId).FirstOrDefault();

            if(cust == null)
            {
                return false;
            }

            cstentity.Entry(cust).CurrentValues.SetValues(entity);
            await cstentity.SaveChangesAsync();
            return true;

        }

        Task<Customers> IRepository<Customers>.GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}