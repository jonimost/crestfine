﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using crestfine.DAL;
using crestfine.Models;
using crestfine.ViewModels;

namespace crestfine.Repositories
{
    public class LoadingOrderRepository : ILoadingOrderRepository
    {
        private CrestfineContext crestfineContext;

        public LoadingOrderRepository()
        {
            crestfineContext = new CrestfineContext();
        }

        public async Task Add(LoadingOrder entity)
        {
            this.crestfineContext.LoadingOrder.Add(entity);
            await this.crestfineContext.SaveChangesAsync();
        }

        public async Task<LoadingOrder> GetById(int id)
        {
            var loadingorder = await this.crestfineContext.LoadingOrder.Where(x => x.Id == id).Select(x => x).FirstOrDefaultAsync();
            return loadingorder;
        }

        public async Task<IDictionary<DateTime, List<LoadingOrder>>> GetDaysLoading(DateTime date)
        {
            var daysloading = await this.crestfineContext.LoadingOrder.Where(x => x.LoadingDay == date).GroupBy(i => i.LoadingDay).ToDictionaryAsync(x => x.Key, l => l.Select(p => p).ToList());
            return daysloading;
        }
        

        public async Task<IDictionary<DateTime, List<LoadingOrder>>> GetList()
        {
            var daysloading = await this.crestfineContext.LoadingOrder.Where(x => x.LoadingDay == DateTime.Today).GroupBy(i => i.LoadingDay).ToDictionaryAsync(x => x.Key, l => l.Select(p => p).ToList());
            return daysloading;
        }

        public async Task<LoadingOrder> GetLoadingOrder(LoadingOrder loading)
        {
            var load = await this.crestfineContext.LoadingOrder.Where(x => x.ProductId == loading.ProductId && x.LoadingDay == loading.LoadingDay).SingleOrDefaultAsync();
            return load;
        }

        public async Task<bool> IsLoaded(DateTime dateTime, int ProductID)
        {
            var loaded = this.crestfineContext.LoadingOrder.Any(x => x.LoadingDay == dateTime && x.ProductId == ProductID);
            return loaded;
        }

        public async Task Remove(int id)
        {
            var loading = await this.crestfineContext.LoadingOrder.Where(x => x.Id == id).FirstOrDefaultAsync();
            this.crestfineContext.LoadingOrder.Remove(loading);
            await this.crestfineContext.SaveChangesAsync();
        }

        public async Task<bool> Update(LoadingOrder entity)
        {
            var loading = this.crestfineContext.LoadingOrder.Where(x => x.Id == entity.Id).FirstOrDefault();

            this.crestfineContext.Entry(loading).CurrentValues.SetValues(entity);
            await this.crestfineContext.SaveChangesAsync();
            return true;
        }
    }
}