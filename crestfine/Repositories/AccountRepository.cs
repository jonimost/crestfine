﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using crestfine.Models;
using crestfine.DAL;
using System.Threading.Tasks;
using System.Data.Entity;
using crestfine.Models.DTO;

namespace crestfine.Repositories
{

    public class AccountRepository : IAccountRepository
    {
        private CrestfineContext crestentity = new CrestfineContext();

        public async Task<AccountSummaryModel> GetAccountSummary(int id)
        {
            var payment = await crestentity.Accounts.Where(acc => acc.CustomerId == id)
                                .Select(x => x.Payment).SumAsync();
            var debt = await crestentity.Accounts.Where(acc => acc.CustomerId == id)
                                .Select(x => x.Debt).SumAsync();

            return new AccountSummaryModel
            {
                Payment = payment,
                Debt = debt,
                Balance = debt - payment
            };

        }

        public async Task Add(Accounts entity)
        {
            crestentity.Accounts.Add(entity);
            await crestentity.SaveChangesAsync();
        }

        public async Task<Accounts> GetById(int id) =>
           await crestentity.Accounts.Where(acc => acc.AccountId == id).FirstOrDefaultAsync();

        //Get Balance 
        public async Task<decimal?> GetPayment(int id)
        {
            //Get payment
            var payment = await crestentity.Accounts.Where(acc => acc.CustomerId == id)
                            .Select(x => x.Payment).SumAsync();

            return payment;
        }

        //Get Debt
        public async Task<decimal?> GetDebt(int id)
        {
            //Get a List of Customers Debt
            var debt = await crestentity.Accounts.Where(acc => acc.CustomerId == id)
                        .Select(x => x.Debt).SumAsync();
            
            return debt;
        }

        public async Task<IEnumerable<Accounts>> GetList() =>
                   await crestentity.Accounts.ToListAsync();

        public async Task Remove(int id)
        {
            var acc_ = crestentity.Accounts.Where(ac => ac.AccountId == id).FirstOrDefault();
            crestentity.Accounts.Remove(acc_);
            await crestentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Accounts entity)
        {
            var acc_ = crestentity.Accounts.Where(acc => acc.AccountId == entity.AccountId).FirstOrDefault();

            if(acc_ == null)
            {
                return false;
            }
            crestentity.Entry(acc_).CurrentValues.SetValues(entity);
            await crestentity.SaveChangesAsync();
            return true;
        }

        //Add the list of purchases that have not been paid
        public async void AddDebt(int id)
        {
            //select Purchase Account
            var purch = crestentity.Purchase.Where(pur => pur.CustomerId == id)
                               .Where(pur => pur.IsPaid == false)
                               .Select(x => x.Payment).Sum();

            var dbt = crestentity.Accounts.Where(d => d.CustomerId == id)
                                .Select(x => x.Debt).ToList();

         
            dbt.Add(purch);
            await crestentity.SaveChangesAsync();
        }

        //Calculate the balance
        public async void Balance(int id)
        {
            decimal? bal= 0.0M;

            var payment = crestentity.Accounts.Where(acc => acc.CustomerId ==id)
                                .Select(x => x.Payment).Sum();

            var debt = crestentity.Accounts.Where(acc => acc.CustomerId == id)
                                .Select(x => x.Debt).Sum();
            var balance = crestentity.Accounts.Where(acc => acc.CustomerId == id)
                                .Select(x => x.Balance).ToList();

            bal = debt - payment;
            balance.Add(bal);
            await crestentity.SaveChangesAsync();
        }
        


        public async Task<decimal?> GetWeekPayment() =>
           await crestentity.Accounts.Where(d => (DateTime.Today - d.Time.Date) == TimeSpan.FromDays(7))
                                .Select(x => x.Payment).SumAsync();

        public async Task<decimal?> GetWeekDebt() =>
           await crestentity.Accounts.Where(d => (DateTime.Today - d.Time.Date) == TimeSpan.FromDays(7))
                                .Select(x => x.Debt).SumAsync();

        public async Task<decimal?> GetMonthPayment() =>
           await crestentity.Accounts.Where(d => (DateTime.Today - d.Time.Date) == TimeSpan.FromDays(28))
                                .Select(x => x.Payment).SumAsync();


        public async Task<decimal?> GetMonthDebt() =>
           await crestentity.Accounts.Where(d => (DateTime.Today - d.Time.Date) == TimeSpan.FromDays(28))
                                .Select(x => x.Debt).SumAsync();


        public async Task<decimal?> GetAnnualPayment() =>
           await crestentity.Accounts.Where(d => (DateTime.Today - d.Time.Date) == TimeSpan.FromDays(365))
                                .Select(x => x.Payment).SumAsync();

        public async Task<decimal?> GetAnnualDebt() =>
           await crestentity.Accounts.Where(d => (DateTime.Today - d.Time.Date) == TimeSpan.FromDays(365))
                                .Select(x => x.Debt).SumAsync();

        public async Task<IEnumerable<Accounts>> GetAccountsCustomerList(int id) =>
           await crestentity.Accounts.Where(acc => acc.CustomerId == id).ToListAsync();
    }
}