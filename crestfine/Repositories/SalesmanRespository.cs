﻿using crestfine.DAL;
using crestfine.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public class SalesmanRespository: ISalesmanRepository
    {
        private CrestfineContext cstentity;

        public SalesmanRespository()
        {
            this.cstentity = new CrestfineContext();
        }

        public async Task<IEnumerable<Salesman>> GetList()
        {
            var saleslist = await cstentity.Salesman.OrderBy(q => q.Name).ToListAsync();
            return saleslist;
        }

        public async Task<Salesman> GetById(int id_)
        {
            var salesman_ = await cstentity.Salesman.Where(id => id.SalesmanId == id_).FirstOrDefaultAsync();
            return salesman_;
        }

        public async Task Add(Salesman salesman)
        {
            cstentity.Salesman.Add(salesman);
            await cstentity.SaveChangesAsync();
        }

        public async Task Remove(int id_)
        {
            var saleman = cstentity.Salesman.Where(id => id.SalesmanId == id_).FirstOrDefault();
            cstentity.Salesman.Remove(saleman);
            await cstentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Salesman entity)
        {
            var salemn = cstentity.Salesman.Where(s => s.SalesmanId == entity.SalesmanId).FirstOrDefault();

            if(salemn == null)
            {
                return false;
            }
            cstentity.Entry(salemn).CurrentValues.SetValues(entity);
            await cstentity.SaveChangesAsync();
            return true;
        }
        
    }
}