﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface ICustomerRepository: IRepository<Customers>
    {
        Task<IEnumerable<Customers>> GetList();
        Task<Customers> GetById(int id);
    }
}