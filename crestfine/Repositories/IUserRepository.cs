﻿using crestfine.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crestfine.Repositories
{
    public interface IUserRepository
    {
        Task<IdentityResult> RegisterUser(UserViewModel users);
        Task<User> FindUser(string email, string password);
        void Dispose();
    }
}
