﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface IRouteRepository:IRepository<Route>
    {
        Task<IEnumerable<Route>> GetList();
        Task<Route> GetById(int id);
    }
}