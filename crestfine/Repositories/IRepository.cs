﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface IRepository<T>
    {
        Task<T> GetById(int id);
        Task Add(T entity);
        Task Remove(int id);
        Task<bool> Update(T entity);
    }
}