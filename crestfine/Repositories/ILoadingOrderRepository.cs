﻿using crestfine.Models;
using crestfine.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crestfine.Repositories
{
    public interface ILoadingOrderRepository: IRepository<LoadingOrder>
    {
        //Task<IDictionary<string, List<LoadingOrder>>> GetLoadingBySalesman(int Id);
        Task<IDictionary<DateTime, List<LoadingOrder>>> GetDaysLoading(DateTime date);
        Task<IDictionary<DateTime, List<LoadingOrder>>> GetList();
        Task<bool> IsLoaded(DateTime dateTime, int ProductID);
        Task<LoadingOrder> GetLoadingOrder(LoadingOrder loading);
    }
}
