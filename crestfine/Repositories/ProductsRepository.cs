﻿using crestfine.DAL;
using crestfine.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public class ProductsRepository:IProductsRepository
    {
        private CrestfineContext cstentity;

        public ProductsRepository()
        {
            this.cstentity = new CrestfineContext();
        }

        public async Task<IEnumerable<Products>> GetList()
        {
            var productlist = await cstentity.Products.OrderByDescending(name => name.Name).ToListAsync();
            return productlist;
        }

        public async Task<Products> GetById(int id_)
        {
            var product = await cstentity.Products.Where(id => id.ProductId == id_).FirstOrDefaultAsync();
            return product;
        }
        public async Task Add(Products prod)
        {
            cstentity.Products.Add(prod);
            await cstentity.SaveChangesAsync();
        }

        public async Task Remove(int id_)
        {
            var product =cstentity.Products.Where(id => id.ProductId == id_).FirstOrDefault();
            cstentity.Products.Remove(product);
            await cstentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Products entity)
        {
            var prod = await cstentity.Products.Where(p => p.ProductId == entity.ProductId).FirstOrDefaultAsync();

            if(prod == null)
            {
                return false;
            }
            cstentity.Entry(prod).CurrentValues.SetValues(entity);
            cstentity.SaveChanges();
            return true;
        }
    }
}