﻿using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface IPurchaseRepository: IRepository<Purchase>
    {
        Task<IEnumerable<Purchase>> GetList();
        Task<Purchase> GetById(int id_);
        Task<IDictionary<DateTime, List<Purchase>>> GetUnpaidPurchases(int id);
        Task<IDictionary<DateTime, List<Purchase>>> GetPaidPurchases(int id);
        Task<IDictionary<DateTime, List<Purchase>>> GetPurchases(int id);
        Task<IDictionary<DateTime, List<Purchase>>> GetPurchasesPastDeadline();
        Task<IDictionary<string, List<Purchase>>> GetDaysPurchases(DateTime dateTime);
        Task<IList<Purchase>> GetDatesPurchase(int id, DateTime dateTime);
        Task<decimal> AverageWeeklyPurchases();
        Task<decimal> AverageMonthlyPurchases();
        Task<decimal> AverageYearlyPurchases();
    }
}