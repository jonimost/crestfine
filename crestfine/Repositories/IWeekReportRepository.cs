﻿using crestfine.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public interface IWeekReportRepository: IRepository<WeekReports>
    {
        Task<IEnumerable<WeekReports>> GetList();
    }
}