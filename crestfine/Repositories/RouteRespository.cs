﻿using crestfine.DAL;
using crestfine.Models;
using crestfine.Models.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Repositories
{
    public class RouteRespository: IRouteRepository
    {
        private CrestfineContext cstentity;

        public RouteRespository()
        {
            this.cstentity = new CrestfineContext();
        }

        public async Task<IEnumerable<Route>> GetList()
        {
            var routes = await cstentity.Route.OrderBy(ord => ord.RouteId).ToListAsync();
            return routes;
        }

        public async Task<Route> GetById(int id_)
        {
            var route_ = await cstentity.Route.Where(id => id.RouteId == id_).FirstOrDefaultAsync();
            
            return route_;
        }

        public async Task Add(Route route_)
        {
            cstentity.Route.Add(route_);
            await cstentity.SaveChangesAsync();
        }
        public async Task Remove(int id_)
        {
            var route_ = cstentity.Route.Where(id => id.RouteId == id_).FirstOrDefault();
            cstentity.Route.Remove(route_);
            await cstentity.SaveChangesAsync();
        }

        public async Task<bool> Update(Route entity)
        {
            var rout = cstentity.Route.Where(r => r.RouteId == entity.RouteId).FirstOrDefault();

            if(rout == null)
            {
                return false;
            }

            cstentity.Entry(rout).CurrentValues.SetValues(entity);
            await cstentity.SaveChangesAsync();
            return true;
        }

        Task<Route> IRepository<Route>.GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}