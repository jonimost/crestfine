namespace crestfine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        Payment = c.Decimal(precision: 18, scale: 2),
                        Debt = c.Decimal(precision: 18, scale: 2),
                        Balance = c.Decimal(precision: 18, scale: 2),
                        Time = c.DateTime(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        PurchaseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountId);
            
            CreateTable(
                "dbo.AnnualReports",
                c => new
                    {
                        AnnualReportID = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        AverageAnnualSales = c.Decimal(precision: 18, scale: 2),
                        Debt = c.Decimal(precision: 18, scale: 2),
                        Amount = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.AnnualReportID)
                .Index(t => t.Time, unique: true);
            
            CreateTable(
                "dbo.Assets",
                c => new
                    {
                        AssetId = c.Int(nullable: false, identity: true),
                        Item = c.String(nullable: false, maxLength: 200),
                        Description = c.String(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.AssetId)
                .Index(t => t.Item, unique: true);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        PhoneNumber = c.String(maxLength: 15),
                        Email = c.String(maxLength: 200),
                        Address = c.String(),
                        RouteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId)
                .ForeignKey("dbo.Route", t => t.RouteId, cascadeDelete: true)
                .Index(t => t.RouteId);
            
            CreateTable(
                "dbo.Purchase",
                c => new
                    {
                        PurchaseId = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        Payment = c.Decimal(precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        User = c.String(),
                    })
                .PrimaryKey(t => t.PurchaseId)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Description = c.String(nullable: false),
                        CrateSize = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ProductId)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Route",
                c => new
                    {
                        RouteId = c.Int(nullable: false, identity: true),
                        RouteName = c.String(nullable: false, maxLength: 200),
                        Details = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RouteId)
                .Index(t => t.RouteName, unique: true);
            
            CreateTable(
                "dbo.Salesman",
                c => new
                    {
                        SalesmanId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Email = c.String(maxLength: 200),
                        PhoneNumber = c.String(nullable: false, maxLength: 15),
                        IDNumber = c.Int(),
                        Address = c.String(),
                        RouteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SalesmanId)
                .ForeignKey("dbo.Route", t => t.RouteId, cascadeDelete: true)
                .Index(t => new { t.PhoneNumber, t.IDNumber }, unique: true, name: "salesmanindex")
                .Index(t => t.RouteId);
            
            CreateTable(
                "dbo.LoadingOrder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SalesmanId = c.Int(nullable: false),
                        RouteId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        LoadedQuantity = c.Int(nullable: false),
                        ReturnedQuantity = c.Int(),
                        LoadingDay = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Route", t => t.RouteId, cascadeDelete: true)
                .ForeignKey("dbo.Salesman", t => t.SalesmanId)
                .Index(t => t.SalesmanId)
                .Index(t => t.RouteId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.MonthlyReports",
                c => new
                    {
                        MonthlyReportID = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        Debt = c.Decimal(precision: 18, scale: 2),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        AverageMonthlySales = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.MonthlyReportID)
                .Index(t => t.Time, unique: true);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        RoleId = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        IdentityUser_UserName = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.IdentityUser_UserName)
                .Index(t => t.RoleId)
                .Index(t => t.IdentityUser_UserName);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserName = c.String(nullable: false, maxLength: 256),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        Id = c.String(),
                        FirstName = c.String(maxLength: 10),
                        LastName = c.String(maxLength: 10),
                        IDNumber = c.String(maxLength: 15),
                        Address = c.String(maxLength: 10),
                        JoinDate = c.DateTime(),
                        Discriminator = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.UserName);
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        IdentityUser_UserName = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.IdentityUser_UserName)
                .Index(t => t.IdentityUser_UserName);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(),
                        ProviderKey = c.String(),
                        IdentityUser_UserName = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.User", t => t.IdentityUser_UserName)
                .Index(t => t.IdentityUser_UserName);
            
            CreateTable(
                "dbo.WeekReports",
                c => new
                    {
                        ReportID = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        Debt = c.Decimal(precision: 18, scale: 2),
                        Amount = c.Decimal(precision: 18, scale: 2),
                        AverageWeekSales = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ReportID)
                .Index(t => t.Time, unique: true);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.UserName)
                .ForeignKey("dbo.User", t => t.UserName)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "UserName", "dbo.User");
            DropForeignKey("dbo.UserRole", "IdentityUser_UserName", "dbo.User");
            DropForeignKey("dbo.UserLogin", "IdentityUser_UserName", "dbo.User");
            DropForeignKey("dbo.UserClaim", "IdentityUser_UserName", "dbo.User");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.LoadingOrder", "SalesmanId", "dbo.Salesman");
            DropForeignKey("dbo.LoadingOrder", "RouteId", "dbo.Route");
            DropForeignKey("dbo.LoadingOrder", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Salesman", "RouteId", "dbo.Route");
            DropForeignKey("dbo.Customers", "RouteId", "dbo.Route");
            DropForeignKey("dbo.Purchase", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Purchase", "CustomerId", "dbo.Customers");
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.WeekReports", new[] { "Time" });
            DropIndex("dbo.UserLogin", new[] { "IdentityUser_UserName" });
            DropIndex("dbo.UserClaim", new[] { "IdentityUser_UserName" });
            DropIndex("dbo.UserRole", new[] { "IdentityUser_UserName" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.Role", "RoleNameIndex");
            DropIndex("dbo.MonthlyReports", new[] { "Time" });
            DropIndex("dbo.LoadingOrder", new[] { "ProductId" });
            DropIndex("dbo.LoadingOrder", new[] { "RouteId" });
            DropIndex("dbo.LoadingOrder", new[] { "SalesmanId" });
            DropIndex("dbo.Salesman", new[] { "RouteId" });
            DropIndex("dbo.Salesman", "salesmanindex");
            DropIndex("dbo.Route", new[] { "RouteName" });
            DropIndex("dbo.Products", new[] { "Name" });
            DropIndex("dbo.Purchase", new[] { "ProductId" });
            DropIndex("dbo.Purchase", new[] { "CustomerId" });
            DropIndex("dbo.Customers", new[] { "RouteId" });
            DropIndex("dbo.Assets", new[] { "Item" });
            DropIndex("dbo.AnnualReports", new[] { "Time" });
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.WeekReports");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.UserRole");
            DropTable("dbo.Role");
            DropTable("dbo.MonthlyReports");
            DropTable("dbo.LoadingOrder");
            DropTable("dbo.Salesman");
            DropTable("dbo.Route");
            DropTable("dbo.Products");
            DropTable("dbo.Purchase");
            DropTable("dbo.Customers");
            DropTable("dbo.Assets");
            DropTable("dbo.AnnualReports");
            DropTable("dbo.Accounts");
        }
    }
}
