namespace crestfine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRouteRelation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LoadingOrder", "Route_RouteId", "dbo.Route");
            DropIndex("dbo.LoadingOrder", new[] { "Route_RouteId" });
            DropColumn("dbo.LoadingOrder", "Route_RouteId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LoadingOrder", "Route_RouteId", c => c.Int());
            CreateIndex("dbo.LoadingOrder", "Route_RouteId");
            AddForeignKey("dbo.LoadingOrder", "Route_RouteId", "dbo.Route", "RouteId");
        }
    }
}
