namespace crestfine.Migrations
{
    using crestfine.DAL;
    using crestfine.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<crestfine.DAL.CrestfineContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(crestfine.DAL.CrestfineContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
             context.Products.AddOrUpdate(x=>x.Name,
                 new Products { Name = "Bread", Description = "Medium Size", CrateSize = 25, Cost = 45 },
                 new Products { Name = "KDF", Description = "Mandazi", CrateSize = 40, Cost = 35 },
                 new Products { Name = "Bread L", Description = "Large Size", CrateSize = 18, Cost = 65 },
                 new Products { Name = "Donut", Description = "Medium-Pack", CrateSize = 45, Cost = 32 }
                 );

            /*context.Roles.AddOrUpdate(
                new IdentityRole { Name = "User" },
                new IdentityRole { Name = "Admin" });*/
        }   
    }
}
