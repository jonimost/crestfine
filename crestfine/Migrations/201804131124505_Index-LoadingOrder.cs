namespace crestfine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IndexLoadingOrder : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.LoadingOrder", new[] { "LoadingDay" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.LoadingOrder", "LoadingDay", unique: true);
        }
    }
}
