namespace crestfine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSalesman_FK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Purchase", "SalesmanId", c => c.Int());
            CreateIndex("dbo.Purchase", "SalesmanId");
            CreateIndex("dbo.LoadingOrder", "LoadingDay", unique: true);
            AddForeignKey("dbo.Purchase", "SalesmanId", "dbo.Salesman", "SalesmanId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Purchase", "SalesmanId", "dbo.Salesman");
            DropIndex("dbo.LoadingOrder", new[] { "LoadingDay" });
            DropIndex("dbo.Purchase", new[] { "SalesmanId" });
            DropColumn("dbo.Purchase", "SalesmanId");
        }
    }
}
