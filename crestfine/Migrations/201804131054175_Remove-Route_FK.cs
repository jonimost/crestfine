namespace crestfine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRoute_FK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LoadingOrder", "RouteId", "dbo.Route");
            DropIndex("dbo.LoadingOrder", new[] { "RouteId" });
            RenameColumn(table: "dbo.LoadingOrder", name: "RouteId", newName: "Route_RouteId");
            AlterColumn("dbo.LoadingOrder", "Route_RouteId", c => c.Int());
            CreateIndex("dbo.LoadingOrder", "Route_RouteId");
            AddForeignKey("dbo.LoadingOrder", "Route_RouteId", "dbo.Route", "RouteId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoadingOrder", "Route_RouteId", "dbo.Route");
            DropIndex("dbo.LoadingOrder", new[] { "Route_RouteId" });
            AlterColumn("dbo.LoadingOrder", "Route_RouteId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.LoadingOrder", name: "Route_RouteId", newName: "RouteId");
            CreateIndex("dbo.LoadingOrder", "RouteId");
            AddForeignKey("dbo.LoadingOrder", "RouteId", "dbo.Route", "RouteId", cascadeDelete: true);
        }
    }
}
