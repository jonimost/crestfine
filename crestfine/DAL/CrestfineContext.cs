﻿using crestfine.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace crestfine.DAL
{
    public class ApplicationUser: IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);

            return userIdentity;
        }
    }

    public partial class CrestfineContext: IdentityDbContext<ApplicationUser>
    {
        public CrestfineContext():
            base("WelbeckDatabase", throwIfV1Schema: false)
        {
            //this.Configuration.LazyLoadingEnabled = true;
        }
        
        //call 
        public static CrestfineContext Create()
        {
            return new CrestfineContext();
        }
        
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Salesman> Salesman { get; set; }
        public virtual DbSet<Route> Route { get; set; }
        public virtual DbSet<Purchase> Purchase { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Assets> Assets { get; set; }
        public virtual DbSet<WeekReports> WKReports { get; set; }
        public virtual DbSet<MonthlyReports> MONReports { get; set; }
        public virtual DbSet<AnnualReports> ANNReports { get; set; }
        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<LoadingOrder> LoadingOrder { get; set; }

        /*public DbSet<IdentityUserLogin> UserLogins { get; set; }
        public DbSet<IdentityUserClaim> UserClaims { get; set; }
        public DbSet<IdentityUserRole> UserRoles { get; set; }*/
        //public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder builder)
        {

            base.OnModelCreating(builder);

            builder.Conventions.Remove<PluralizingTableNameConvention>();
            builder.Entity<LoadingOrder>().HasRequired(c => c.Salesman).WithMany().WillCascadeOnDelete(false);
            builder.Entity<IdentityUserClaim>().ToTable("UserClaim").HasKey<Int32>(r => r.Id);
            builder.Entity<IdentityUserLogin>().ToTable("UserLogin").HasKey<string>(l => l.UserId);
            builder.Entity<IdentityRole>().ToTable("Role").HasKey<string>(r => r.Id);
            builder.Entity<User>().ToTable("User").HasKey(r => new{ r.IDNumber, r.UserName});
            builder.Entity<IdentityUser>().ToTable("User").HasKey<string>(r => r.UserName);
            builder.Entity<IdentityUserRole>().ToTable("UserRole").HasKey(r => new { r.RoleId, r.UserId });
            /**/

           // 
        }
        

        protected override void Dispose(bool disposing)
        {

            base.Dispose(disposing);
        }
    }
}