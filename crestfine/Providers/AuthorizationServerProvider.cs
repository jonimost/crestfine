﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Owin.Security;
using crestfine.DAL;
using System.Threading;
using crestfine.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using crestfine.App_Start;
using System.Web.Http;
using crestfine.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;

namespace crestfine.Providers
{
    public class AuthorizationServerProvider: OAuthAuthorizationServerProvider
    {
       
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

                //User user = await usrrepo.FindUser(context.UserName, context.Password);
                var userManager = context.OwinContext.GetUserManager<AppUserManager>();
                User user = await userManager.FindAsync(context.UserName, context.Password);

                if(user == null)
                {
                    context.SetError("Invalid_grant", "either the password or username is wrong");
                    return;
                }
            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,OAuthDefaults.AuthenticationType);
            
            Claim roles = oAuthIdentity.Claims.Where(x => x.Type == ClaimTypes.Role).FirstOrDefault();

            AuthenticationProperties properties = CreateProperties(roles.Value,user.Id);
           
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {

            foreach(KeyValuePair<string,string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }


        public static AuthenticationProperties CreateProperties(string roles, string userId)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                {"role",roles },
                {"userId", userId }
            };
            return new AuthenticationProperties(data);
        }
        
    }
}