﻿using crestfine.App_Start;
using crestfine.DAL;
using crestfine.Models;
using crestfine.Repositories;
using crestfine.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Controllers
{
    [RoutePrefix("api/useraccount")]
    public class AccountController : ApiController
    {
        private AppUserManager userManager;
        //private AppRoleManager roleManager;
        private ModelFactory modelFactory;

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }
        protected ModelFactory ModelFactory {
            get
            {
                if(modelFactory == null)
                {
                    modelFactory = new ModelFactory(this.Request, this.UserManager);
                }
                return modelFactory;
            } }


        public AccountController()
        {
        }


        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        public AccountController(AppUserManager userManager,
          ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public AppUserManager UserManager
        {
            get
            {
                return userManager ?? Request.GetOwinContext().GetUserManager<AppUserManager>();
            }
            private set
            {
                userManager = value;
            }
        }


        [Authorize(Roles="Admin")]
        [Route("register")]
        [HttpPost]
        public async Task<IHttpActionResult> Register(UserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new User() { UserName = model.Email, Email = model.Email, IDNumber = model.IDNumber,
                                    FirstName=model.FirstName,LastName=model.LastName, Address=model.Address, PhoneNumber=model.PhoneNumber, JoinDate=DateTime.Today};
            var manager = new UserManager<User>(new UserStore<User>(new CrestfineContext()));

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);
            var usr = await UserManager.FindByNameAsync(model.Email);
             
            //I NOW HAVE TO DO THIS SHIT
            if(user.UserName == "alex@wellbake.co.ke")
            {
                var rolemanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new CrestfineContext()));
                
                rolemanager.Create(new IdentityRole { Name = "Admin" });
                rolemanager.Create(new IdentityRole { Name = "User" });
                manager.AddToRole(usr.Id, "Admin");
            }
            else
            {
                
                manager.AddToRole(usr.Id, "User");
            }


            if (!result.Succeeded)
            {
                return GetError(result);
            }

            return Ok();
        }

        [Authorize(Roles ="Admin")]
        [Route("users")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUsers()
        {
            var user = new AppUserManager(new UserStore<User>(new CrestfineContext()));
            return Ok(user.Users.ToList().Select(x => ModelFactory.Users(x)));
        }

        [Authorize(Roles ="Admin, User")]
        [Route("users/{Id:guid}", Name = "GetUserInfo")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUserbyId(string Id)
        {
            var manager = new AppUserManager(new UserStore<User>(new CrestfineContext()));
            var user = await manager.FindByIdAsync(Id);

            if(user == null)
            {
                return NotFound();
            }
            return Ok(this.ModelFactory.Create(user));
        }

        
        
        [Authorize(Roles ="Admin")]
        [Route("edit/{Id:guid}")]
        [HttpPut]
        public async Task<IHttpActionResult> Edit(string Id,UserBindingModel user)
        {
            CrestfineContext context = new CrestfineContext();
            if (ModelState.IsValid)
            {
                User user_ = await UserManager.FindByIdAsync(Id);
                user_.IDNumber = user.IDNumber;
                user_.FirstName = user.FirstName;
                user_.LastName = user.LastName;
                user_.PhoneNumber = user.PhoneNumber;
                user_.Address = user.Address;
                user_.Email = user.Email;
                user_.UserName = user.Email;

                IdentityResult result = await UserManager.UpdateAsync(user_);
                await context.SaveChangesAsync();

                return Content(HttpStatusCode.Accepted, result);
            }
            return BadRequest("Wrong Input");
        }

        [Authorize(Roles ="Admin")]
        [Route("delete/{Id:guid}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(string Id)
        {
            var manager = new AppUserManager(new UserStore<User>(new CrestfineContext()));

            User user = await manager.FindByIdAsync(Id);
            if(user != null)
            {
                IdentityResult result = await manager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    return GetError(result);
                }
            }
            return Ok();
        }

        [Authorize(Roles ="Admin,User")]
        [Route("~/api/useraccount/{Id:guid}/changepassword")]
        [HttpPost]
        public async Task<IHttpActionResult> ChangePassword(string Id, ChangePassword changePassword)
        {
            if (ModelState.IsValid)
            {
                User usr = await UserManager.FindByIdAsync(Id);
                usr.PasswordHash = UserManager.PasswordHasher.HashPassword(changePassword.Newpassword);
                await UserManager.UpdateAsync(usr);

                return Ok();

            }
            return BadRequest();
        }

        public IHttpActionResult GetError(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}