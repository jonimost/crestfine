﻿using crestfine.App_Start;
using crestfine.DAL;
using crestfine.Models;
using crestfine.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace crestfine.Controllers
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/roles")]
    public class RolesController : ApiController
    {
        private CrestfineContext context = new CrestfineContext();
        private ModelFactory modelFactory;
        private AppUserManager userManager = null;
        private AppRoleManager roleManager = null;


        public AppRoleManager RoleManager
        {
            get
            {
                return roleManager ?? Request.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }

        public AppUserManager UserManager
        {
            get
            {
                return userManager ?? Request.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        protected ModelFactory ModelFactory
        {
            get
            {
                if (modelFactory == null)
                {
                    modelFactory = new ModelFactory(this.Request, this.RoleManager);
                }
                return modelFactory;
            }
        }


        [HttpGet]
        [Route("{id:guid}", Name = "GetRolebyId")]
        public async Task<IHttpActionResult> GetRolebyId(string id)
        {
            IdentityRole role = await this.RoleManager.FindByIdAsync(id);

            if (role == null)
            {
                return NotFound();
            }
            return Ok(ModelFactory.Create(role));
        }

        [Route("")]
        [HttpGet]
        public async Task<IHttpActionResult> GetRoles()
        {
            var roles = this.RoleManager.Roles.ToList();
            List<RolesModel> rolelst = new List<RolesModel>(); 
            foreach(var role in roles)
            {
                rolelst.Add(ModelFactory.Roles(role));
                
            }
            return Ok(rolelst);
        }

        [Route("{id:guid}/users")]
        [HttpGet]
        public async Task<IHttpActionResult> GetUsersinRole(string id)
        {
         
            IdentityRole role = await this.RoleManager.FindByIdAsync(id);
            if(role == null)
            {

                return NotFound();
            }
            IList<User> usersinRole = new List<User>();
            List<User> users = this.UserManager.Users.ToList();

            foreach(var user in users)
            {
                if(await this.UserManager.IsInRoleAsync(user.Id, role.Name))
                {
                    //map
                    
                    usersinRole.Add(user);
                }
            }

            return Ok(usersinRole.Select(x => ModelFactory.UserRole(x)));

        }


        [Route("addtorole")]
        [HttpPost]
        public async Task<IHttpActionResult> AddUserstoRole(UsersInRoleModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var role = await this.RoleManager.FindByIdAsync(model.Id);

            if(role == null)
            {
                ModelState.AddModelError("", "Role doesn't exist");
                return BadRequest(ModelState);
            }

            foreach(var user in model.Users)
            {
                var regUser = await this.UserManager.FindByNameAsync(user);
                
                if(regUser == null)
                {
                    ModelState.AddModelError("", $"User: {user} doesn't exist");
                    continue;
                }

                if(! await this.UserManager.IsInRoleAsync(regUser.Id, role.Name))
                {
                    IdentityResult result = await this.UserManager.AddToRoleAsync(regUser.Id, role.Name);
                    //this.context.Entry(regUser).State = EntityState.Modified;
                    //await this.context.SaveChangesAsync();
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", $"Unable to add {user} to {role.Name} Role");
                        
                    }
                }
            }

            return Ok();
        }

        [Route("removefromrole/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> RemovefromRole(string id,RemoveUserFromRole model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var role = await this.RoleManager.FindByIdAsync(id);

            if(role == null)
            {
                ModelState.AddModelError("", "Role Doesn't Exist");
                return BadRequest(ModelState);
            }

            foreach(var user in model.Users)
            {
                var regUser = await this.UserManager.FindByNameAsync(user);

                if(regUser == null)
                {
                    ModelState.AddModelError("", $"{user} doesn't exist");
                    continue;
                }

                if(await this.UserManager.IsInRoleAsync(regUser.Id, role.Name))
                {
                    IdentityResult result = await this.UserManager.RemoveFromRoleAsync(regUser.Id, role.Name);
                   
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("nouser", $"Couldn't remove {user} from Role");
                        //return BadRequest("nouser");
                    }
                }
            }

            return Ok();
        }


        public IHttpActionResult GetError(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }
            return null;
        }
    }
}
