﻿using crestfine.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    public partial class AnnualReports
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AnnualReportID { get; set; }
        [Index(IsUnique =true)]
        public DateTime Time { get; set; }
        public decimal? AverageAnnualSales { get; set; }
        public decimal? Debt { get; set; }
        public decimal? Amount { get; set; }
    }
}