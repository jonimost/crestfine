﻿using crestfine.App_Start;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace crestfine.Models
{
    public class AppUserLogin : IdentityUserLogin<string> { }
    public class AppUserClaim : IdentityUserClaim<string> { }
    public class AppUserRole : IdentityUserRole<string> { }

    public partial class User : IdentityUser
    {
        [Required]
        [StringLength(10)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(10)]
        public string LastName { get; set; }
        [Required]
        [StringLength(15)]
        public string IDNumber { get; set; }
        [Required]
        [StringLength(10)]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime JoinDate { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(AppUserManager userManager, string authenticationType)
        {
            var userIdentity = await userManager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }

    }

}