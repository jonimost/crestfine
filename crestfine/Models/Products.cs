﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace crestfine.Models
{
    /// <summary>
    /// Product Details
    /// </summary>
    public partial class Products
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }
        [Required]
        [StringLength(200)]
        [Index(IsUnique =true)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int CrateSize { get; set; }
        [Required]
        public decimal Cost { get; set; }
    }
}