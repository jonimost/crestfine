﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crestfine.Models.Enums
{
    public enum Invoice
    { 
        UNPAID,
        PAID,
        CANCELLED
    }
}
