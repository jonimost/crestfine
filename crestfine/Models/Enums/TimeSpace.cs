﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.Enums
{
    public enum TimeSpace
    {
        Weekly =7,
        Monthly = 28,
        Annually =365
    }
}