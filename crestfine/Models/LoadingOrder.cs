﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    public partial class LoadingOrder
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id {get; set; }
        //[Required]
        public int? SalesmanId { get; set; }
        //[Required]
        //public int RouteId { get; set; }
        [Required]
        public int ProductId { get; set; }
        [Required]
        public int LoadedQuantity { get; set; }
        public int? ReturnedQuantity { get; set; }
        [Required]
        public DateTime LoadingDay { get; set; }
        public string User { get; set; }

    }

    [Serializable]
    public partial class LoadingOrder
    {
        //[JsonIgnore]
        //[ForeignKey("SalesmanId")]
        public virtual Salesman Salesman { get; set; }
        //[JsonIgnore]
        //[ForeignKey("ProductId")]
        public virtual Products Products { get; set; }
    }
}