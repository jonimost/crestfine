﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.DTO
{
    [Serializable]
    public partial class PurchaseModel
    {
        public int PurchaseId { get; set; }
        public int Quantity { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Payment { get; set; }
        public DateTime Date { get; set; }
        public bool IsPaid { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int? SalesmanId { get; set; }
        public string User { get; set; }
        
       // [JsonIgnore]
        public virtual Products Products { get; set; }
      //  [JsonIgnore]
        public virtual Customers Customer { get; set; }
        public virtual Salesman Salesman { get; set; }
    }

}