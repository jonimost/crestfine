﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.DTO
{
    public class AccountSummaryModel
    {
        public decimal? Payment { get; set; }
        public decimal? Debt { get; set; }
        public decimal? Balance { get; set; }
    }
}