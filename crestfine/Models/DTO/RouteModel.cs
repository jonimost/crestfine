﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.DTO
{
    //[Serializable]
    public class RouteModel
    {
        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public string Details { get; set; }

       // [JsonIgnore]
        public virtual ICollection<Salesman> Salesman { get; set; }
       // [JsonIgnore]
        public virtual ICollection<Customers> Customers { get; set; }
    }
}
