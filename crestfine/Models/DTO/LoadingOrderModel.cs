﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.DTO
{
    //[Serializable]
    public partial class LoadingOrderModel
    {
          public int Id { get; set; }
          public int SalesmanId { get; set; }
          public int ProductId { get; set; }
          //public int RouteId { get; set; }
          public int LoadedQuantity { get; set; }
          public int? ReturnedQuantity { get; set; }
          public DateTime LoadingDay { get; set; }
          public string User { get; set; }

    }
    
    public partial class LoadingOrder
    {
          public virtual Salesman Salesman { get; set; }
    
          public virtual Products Products { get; set; }
        }
}
