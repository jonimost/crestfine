﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.DTO
{
    public class AccountModel
    {
        public int AccountId { get; set; }
        public decimal? Payment { get; set; }
        public decimal? Debt { get; set; }
        public decimal? Balance { get; set; }
        public DateTime Time { get; set; }
        public int CustomerId { get; set; }
        public int PurchaseId { get; set; }
    }
}