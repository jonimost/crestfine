﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace crestfine.Models.DTO
{
    //[Serializable]
    public partial class CustomerModel
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int RouteId { get; set; }

    }
    [Serializable]
    public partial class CustomerModel
    {
        [JsonIgnore]
        public ICollection<Purchase> Purchases { get; set; }
        //[JsonIgnore]
        public Route Route { get; set; }
    }
}