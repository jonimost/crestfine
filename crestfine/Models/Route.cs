﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    /// <summary>
    /// Route Details
    /// </summary>
    
    public partial class Route
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RouteId { get; set; }
        [Required]
        [StringLength(200)]
        [Index(IsUnique =true)]
        public string RouteName { get; set; }
        [Required]
        public string Details { get; set; }

       
    }
    [Serializable]
    partial class Route
    { 
        [JsonIgnore]
        public virtual ICollection<Customers> Customers { get; set; }
        [JsonIgnore]
        public virtual ICollection<Salesman> Salesman { get; set; }
    }
}