﻿using crestfine.Models.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    /// <summary>
    /// Store Calculated cost and track
    /// </summary>
    public partial class Purchase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PurchaseId { get; set; }
        public int Quantity { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Payment { get; set; }
        public DateTime Date { get; set; }
        public bool IsPaid { get; set; }
        public int CustomerId { get; set; }
        public int? SalesmanId { get; set; }
        public int ProductId { get; set; }
        public string User { get; set; }
    }
    [Serializable]
    public partial class Purchase
    {
        [JsonIgnore]
        public virtual Products Products { get; set; }
        [JsonIgnore]
        public virtual Customers Customer { get; set; }
        [JsonIgnore]
        public virtual Salesman Salesman { get; set; }
    }
}