﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    /// <summary>
    /// Assets Details
    /// </summary>
    public partial class Assets
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssetId { get; set; }
        [Required]
        [StringLength(200)]
        [Index(IsUnique =true)]
        public string Item { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int? Quantity { get; set; }
        [Required]
        public decimal Value { get; set; }
    }
}