﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    /// <summary>
    /// Salesmen Details
    /// </summary>
    public partial class Salesman
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SalesmanId { get; set; }
        [Required]
        public string Name { get; set; }
        [StringLength(200)]
        public string Email { get; set; }
        [Required]
        [Index("salesmanindex",Order =2,IsUnique = true)]
        [DataType(DataType.PhoneNumber)]
        [StringLength(15)]
        public string PhoneNumber { get; set; }
        [Index("salesmanindex",Order =3,IsUnique = true)]
        public int? IDNumber { get; set; }
        public string Address { get; set; }
        public int RouteId { get; set; }

        public virtual Route Route { get; set; }
    }
}