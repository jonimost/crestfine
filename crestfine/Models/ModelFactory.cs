﻿using crestfine.App_Start;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;

namespace crestfine.Models
{
    public class ModelFactory
    {
        private UrlHelper urlHelper;
        private AppUserManager appUserManager;
        private AppRoleManager approleManager;

        public ModelFactory(HttpRequestMessage httpRequest, AppUserManager appUserManager)
        {
            this.appUserManager = appUserManager;
            urlHelper = new UrlHelper(httpRequest);
        }

        public ModelFactory(HttpRequestMessage httpRequest, AppRoleManager approleManager)
        {
            this.approleManager = approleManager;
            urlHelper = new UrlHelper(httpRequest);
        }

        public UserModel Create(User user)
        {
            return new UserModel
            {
                Url = urlHelper.Link("GetUserInfo", new { id = user.Id }),
                Fullname = String.Format($"{user.FirstName} {user.LastName}"),
                FirstName=user.FirstName,
                LastName=user.LastName,
                PhoneNumber=user.PhoneNumber,
                IDNumber = user.IDNumber,
                Address = user.Address,
                JoinDate = user.JoinDate,
                Roles = appUserManager.GetRolesAsync(user.Id).Result,
                Id=user.Id,
                Username = user.UserName
            };
        }

        public RoleModel Create(IdentityRole role)
        {
            return new RoleModel
            {
                Url = urlHelper.Link("GetRolebyId", new { id = role.Id }),
                Name = role.Name,
                Id = role.Id
            };
        }

        public RolesModel Roles(IdentityRole role)
        {
            return new RolesModel
            {
                Id = role.Id,
                Name = role.Name
            };
        }

        public UsersModel Users(User users)
        {
            return new UsersModel()
            {
                Fullname = String.Format($"{users.FirstName} {users.LastName}"),
                FirstName=users.FirstName,
                LastName=users.LastName,
                PhoneNumber=users.PhoneNumber,
                IDNumber = users.IDNumber,
                Address = users.Address,
                JoinDate = users.JoinDate,
                Roles = appUserManager.GetRolesAsync(users.Id).Result,
                Id=users.Id,
                Username=users.UserName
            };
        }

        public UserRole UserRole(User user)
        {
            return new UserRole()
            {
                Fullname = String.Format($"{user.FirstName} {user.LastName}"),
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                IDNumber = user.IDNumber,
                Address = user.Address,
                JoinDate = user.JoinDate,
                Id = user.Id,
                Username = user.UserName
            };
        }

       
    }

    public class UserModel
    {
        public string Url { get; set; }
        public string Fullname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string IDNumber { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public DateTime JoinDate { get; set; }
        public IList<string> Roles { get; set; }
        public string Id { get; set; }
        
    }

    public class UserRole
    {
        public string Fullname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string IDNumber { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public DateTime JoinDate { get; set; }
        public string Id { get; set; }
    }

    public class UsersModel
    {
        public string Id { get; set; }
        public string Fullname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string IDNumber { get; set; }
        public string Username { get; set; }
        public string Address { get; set; }
        public DateTime JoinDate { get; set; }
        public IList<string> Roles { get; set; }
        
    }

    public class RoleModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class RolesModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    
}