﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    /// <summary>
    /// Customers Details
    /// </summary>
    public partial class Customers
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerId { get; set; }
        [Required]
        public string Name { get; set; }
        [DataType(DataType.PhoneNumber)]
        [StringLength(15)]
        public string PhoneNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        [StringLength(200)]
        public string Email { get; set; }
        public string Address { get; set; }
        [Required]
        public int RouteId { get; set; }

    }
    [Serializable]
    public partial class Customers{
        [JsonIgnore]
        public ICollection<Purchase> Purchases { get; set; }
        [JsonIgnore]
        public Route Route { get; set; }
    }
}