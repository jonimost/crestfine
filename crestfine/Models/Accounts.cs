﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace crestfine.Models
{
    public partial class Accounts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AccountId { get; set; }
        public decimal? Payment { get; set; }
        public decimal? Debt { get; set; }
        public decimal? Balance { get; set; }
        public DateTime Time { get; set; }
        public int CustomerId { get; set; }
        public int PurchaseId { get; set; }

    }
}