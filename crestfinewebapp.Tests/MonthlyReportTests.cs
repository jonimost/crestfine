﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using crestfinewebapp.Controllers;
using crestfinewebapp.Repositories.IRepository;
using crestfinewebapp.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace crestfinewebapp.Tests
{
    //[TestClass]
    public class MonthlyReportTests
    {
        [Fact]
        public async Task GetMonthlyReportsAsync()
        {
            var mockrepo = new Mock<IDatabaseSelectReports>();
            mockrepo.Setup(i => i.GetMonthlyReports()).ReturnsAsync(GetReports());
            var controller = new MonthlyReportController(mockrepo.Object);

            ActionResult result = await controller.Index(null,null,null,null);

            //assert
            var viewresult = Xunit.Assert.IsType<ViewResult>(result);
            var model = Xunit.Assert.IsAssignableFrom<IEnumerable<MonthlyReport>>(viewresult.ViewData.Model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(2, model.Count());
        }

        [Fact]
        public async Task GetMonthlyReportAsync()
        {
            var mockrepo = new Mock<IDatabaseSelectReports>();
            mockrepo.Setup(i => i.GetMonthReport(1)).ReturnsAsync(GetReport());
            var controller = new MonthlyReportController(mockrepo.Object);

            ActionResult result = await controller.Details(1);

            //assert
            var viewresult = Xunit.Assert.IsType<ViewResult>(result);
            var model = Xunit.Assert.IsAssignableFrom<MonthlyReport>(viewresult.ViewData.Model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(1, model.MonthlyReportID);
        }

        private IList<MonthlyReport> GetReports()
        {
            var monthlyRpts = new List<MonthlyReport>()
            {
                new MonthlyReport{MonthlyReportID=1, Amount=2500, Debt=2100, AverageMonthlySales=15, Time=DateTime.Today},
                new MonthlyReport{MonthlyReportID=2, Amount=15200, Debt=1600, AverageMonthlySales=75, Time=DateTime.Today}
            };

            return monthlyRpts;
        }

        private MonthlyReport GetReport()
        {
            return new MonthlyReport
            {
                MonthlyReportID = 1,
                Amount = 15200,
                Debt = 100,
                AverageMonthlySales = 15,
                Time = DateTime.Today
            };
        }
    }
}
