﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using crestfinewebapp.Controllers;
using crestfinewebapp.Repositories.IRepository;
using crestfinewebapp.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace crestfinewebapp.Tests
{

    public class AnnualReportTests
    {
        [Fact]
        public async Task GetAnnualReportsAsync()
        {
            var mockrepo = new Mock<IDatabaseSelectReports>();
            mockrepo.Setup(i => i.GetAnnualReports()).ReturnsAsync(GetReports());
            var controller = new AnnualReportController(mockrepo.Object);

            ActionResult result = await controller.Index(null,null,null,null);

            //assert
            var viewresult = Xunit.Assert.IsType<ViewResult>(result);
            var model = Xunit.Assert.IsAssignableFrom<IEnumerable<AnnualReport>>(viewresult.ViewData.Model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(2, model.Count());
        }

        [Fact]
        public async Task GetAnnualReportAsync()
        {
            var mockrepo = new Mock<IDatabaseSelectReports>();
            mockrepo.Setup(i => i.GetAnnualReport(1)).ReturnsAsync(GetReport);
            var controller = new AnnualReportController(mockrepo.Object);

            ActionResult result = await controller.Details(1);

            //assert value
            var viewresult = Xunit.Assert.IsType<ViewResult>(result);
            var model = Xunit.Assert.IsAssignableFrom<AnnualReport>(viewresult.ViewData.Model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(1, model.AnnualReportID);
        }

        private IList<AnnualReport> GetReports()
        {
            var annualrpts = new List<AnnualReport>()
            {
                new AnnualReport{AnnualReportID=1, Amount=25000, Debt=14000, AverageAnnualSales=85, Time=DateTime.Today},
                new AnnualReport{AnnualReportID=2, Amount=75000000, Debt=15000, AverageAnnualSales=8000, Time=DateTime.Today}
            };
            return annualrpts;
        }

        private AnnualReport GetReport()
        {
            return new AnnualReport
            {
                AnnualReportID = 1,
                Amount = 25000000,
                Debt = 1400,
                AverageAnnualSales = 8000,
                Time = DateTime.Today
            };
        }
    }
}
