﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using crestfinewebapp.Controllers;
using crestfinewebapp.Repositories.IRepository;
using crestfinewebapp.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit;

namespace crestfinewebapp.Tests
{
    
    public class WeekReportTests
    {
        [Fact]
        public async Task GetWeekReportsAsync()
        {
            var mockrepo = new Mock<IDatabaseSelectReports>();
            mockrepo.Setup(repo => repo.GetWeekReports()).ReturnsAsync(GetReports());
            var controller = new WeekReportController(mockrepo.Object);

            ActionResult result = await controller.Index(null,null,null,null);

            //assert
            var viewresult = Xunit.Assert.IsType<ViewResult>(result);
            var model = Xunit.Assert.IsAssignableFrom<IEnumerable<WeekReport>>(viewresult.ViewData.Model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(2, model.Count());
        }

        [Fact]
        public async Task GetWeekReportAsync()
        {
            var mockrepo = new Mock<IDatabaseSelectReports>();
            mockrepo.Setup(repo => repo.GetWeekReport(1)).ReturnsAsync(GetReport());
            var controller = new WeekReportController(mockrepo.Object);

            ActionResult result = await controller.Details(1);

            //assert
            var viewresult = Xunit.Assert.IsType<ViewResult>(result);
            var model = Xunit.Assert.IsAssignableFrom<WeekReport>(viewresult.ViewData.Model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsNotNull(model);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(1, model.ReportID);

        }

        private WeekReport GetReport()
        {
            return new WeekReport
            {
                ReportID = 1,
                Amount = 2500,
                Debt = 1500,
                AverageWeekSales = 15,
                Time = DateTime.Today
            };
        }

        private IList<WeekReport> GetReports()
        {
            var wkreports = new List<WeekReport>
            {
                new WeekReport{ReportID=1, Amount=2500, Debt=1500, AverageWeekSales=25, Time=DateTime.Today},
                new WeekReport{ReportID=2, Amount=352200, Debt=7511, AverageWeekSales=102, Time=DateTime.Today}
            };

            return wkreports;
        }

        
    }
}
